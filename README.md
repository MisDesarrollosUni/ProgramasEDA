# 4C | ProgramasEDA
## Estructura de Datos Aplicadas | IntelliJ 2020.1 🐱‍👤⚙️
> Saldaña Espinoza Hector - Desarrollo de Software
>

### Contenido 🚀
#### Unidad 1 😎
* [Clase 1](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_1) - Clases beans.
* [Clase 2](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_2) - Clases abstractas, interfaces y enumeradores.
* [Clase 3](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_3) - Recursividad.
* [Clase 4](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_4) - Recursividad.
#### Unidad 2 😇
* [Clase 5](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_5) - Arreglos Unidimensionales.
* [Clase 6](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_6) - Arreglos Unidimensionales.
* [Clase 7](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_7) - Arreglos Bidimensionales.
* [Clase 8](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_8) - Arreglos Bidimensionales.
* [Clase 9](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_9) - Arreglos Bidimensionales.
* [Clase 10](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_10) - Método Burbuja.
* [Clase 11](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_11) - Método Burbuja Ejercicio y Metodo Shell Sort.
* [Clase 12](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_12) - Búsqueda binaria y secuencial.
* [Clase 13](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_13) - Ejercicios Clase 11,12 y 13.
* [Clase 14](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_14) - Examen arreglos: Serpiente y Escaleras - Naval Battle.
#### Unidad 3 😁
* [Clase 15](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_15) - Lista enlazada.
* [Clase 16](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_16) - Lista doblemente enlazada.
* [Clase 17](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_17) - Lista circular.
* [Clase 18](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_18) - Examen listas: Gato.

#### Unidad 4 😐
* [Clase 19](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_19) - Pilas.
* [Clase 20](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_20) - Ejercicio pilas.
* [Clase 21](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_21) - Más ejercicios pilas.
* [Clase 22](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_22) - Más más ejercicios pilas.
* [Clase 23](https://github.com/HectorSaldes/ProgramasEDA/tree/master/src/mx/edu/utez/Clase_23) - Colas
