package mx.edu.utez.Clase_1;

public class Aguinaldo {
	private String frutas;
	private String dulces;

	public Aguinaldo(String frutas, String dulces) {
		this.frutas = frutas;
		this.dulces = dulces;
	}

	public Aguinaldo() {
	}

	public String getFrutas() {
		return frutas;
	}

	public void setFrutas(String frutas) {
		this.frutas = frutas;
	}

	public String getDulces() {
		return dulces;
	}

	public void setDulces(String dulces) {
		this.dulces = dulces;
	}
}
