package mx.edu.utez.Clase_1;

public class Estudiante {
	private String matricula;
	private String nombreCompleto;
	private int cuatri;
	private char grupo;

	public Estudiante(String matricula, String nombreCompleto, int cuatri, char grupo) {
		this.matricula = matricula;
		this.nombreCompleto = nombreCompleto;
		this.cuatri = cuatri;
		this.grupo = grupo;
	}

	public Estudiante() {
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public int getCuatri() {
		return cuatri;
	}

	public void setCuatri(int cuatri) {
		this.cuatri = cuatri;
	}

	public char getGrupo() {
		return grupo;
	}

	public void setGrupo(char grupo) {
		this.grupo = grupo;
	}
}
