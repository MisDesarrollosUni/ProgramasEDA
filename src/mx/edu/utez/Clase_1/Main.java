package mx.edu.utez.Clase_1;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		Aguinaldo aguinaldo1 = new Aguinaldo("Manzanas", "Paletas");
		Aguinaldo aguinaldo2 = new Aguinaldo("Cacahuates", "Miguelito");

		Aguinaldo aguinaldo3 = new Aguinaldo();
		aguinaldo3.setDulces("Picafresas");
		aguinaldo3.setFrutas("Guayaba");

		List<Aguinaldo> aguinaldos = new ArrayList<>();
		aguinaldos.add(aguinaldo1);
		aguinaldos.add(aguinaldo2);
		aguinaldos.add(aguinaldo3);

		aguinaldos.add(new Aguinaldo("Tejocote", "Mazapan"));

		System.out.println("ForEach");
		for (Aguinaldo aguinaldo : aguinaldos) {
			System.out.println("Aguinaldo -> " + aguinaldo.getDulces() + " " + aguinaldo.getFrutas());
		}

		System.out.println("For");
		for (int i = 0; i < aguinaldos.size(); i++) {
			System.out.println("Aguinaldo -> " + aguinaldos.get(i).getFrutas() + " " + aguinaldos.get(i).getDulces());
		}

	}
}
