package mx.edu.utez.Clase_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainEstudiante {
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		List<Estudiante> estudiantes = new ArrayList<>();
		leer.useDelimiter("\n");
		Estudiante estudiante1 = new Estudiante();
		Estudiante estudiante2 = new Estudiante("20193tn071", "Jair David Vasquez Martinez", 4, 'C');

		System.out.print("Ingrese su Matrícula: ");
		estudiante1.setMatricula(leer.next());
		System.out.print("Ingresa tu Nombre Completo: ");
		estudiante1.setNombreCompleto(leer.next());
		System.out.print("Ingrese su Cuatrimestre: ");
		estudiante1.setCuatri(leer.nextInt());
		System.out.print("Ingrese su Grupo: ");
		estudiante1.setGrupo(leer.next().charAt(0));
		estudiantes.add(estudiante1);
		estudiantes.add(estudiante2);

		System.out.println("");
		for (Estudiante e : estudiantes) {
			System.out.println(e.getMatricula() + " | " + e.getNombreCompleto() + " | " + e.getCuatri() + e.getGrupo());
		}

	}
}
