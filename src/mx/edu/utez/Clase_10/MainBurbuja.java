package mx.edu.utez.Clase_10;

public class MainBurbuja {
	public static void main(String[] args) {
		MetodoBurbuja mb = new MetodoBurbuja();
		int arreglo[] = {6, 5, 7, 3, 78, 8, 4, 1, 9, 2, 96};
		arreglo = mb.metodoBurbuja(arreglo);
		for (int num : arreglo) {
			System.out.print(num + " ");
		}
	}
}
