package mx.edu.utez.Clase_10;

public class MainUnirArreglo {

	public static void main(String[] args) {
		UnirArreglo u = new UnirArreglo();
		int a[] = {1, 2, 3, 4, 5};
		int b[] = {6, 7, 8, 9, 10};
		//Merge es para unir dos arreglos que sean del mismo tipo
		int c[] = u.mergeArray(a, b);
		for (int num :c) {
			System.out.print(num+" ");
		}
	}
}
