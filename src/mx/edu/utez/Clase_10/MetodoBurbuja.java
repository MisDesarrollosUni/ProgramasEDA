package mx.edu.utez.Clase_10;

public class MetodoBurbuja {
	public int[] metodoBurbuja(int array[]) {
		int aux = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length - 1; j++) {
				if (!(array[i] < array[j + 1])) {
					aux = array[i];
					array[i] = array[j + 1];
					array[j + 1] = aux;
				}
			}
		}
		return array;
	}
}
