package mx.edu.utez.Clase_10;

public class SplitArreglo {

	public int[] split(int[] numeros, int i) {
		int division[] = null;
		int c = 1;
		int valor = 0;
		for (int j = 0; j < numeros.length; j++) {
			if (numeros[j] == i){
				c++;
				valor = j;
			}
		}
		if(c==numeros.length-1){
			c=0;
		}
		if(c==1 && valor != numeros.length-1 ){
			c++;
		}
		division = new int[c];
		int pos = -1;
		for (int j = 0; j < division.length; j++) {
			for (int k = pos + 1; k < numeros.length; k++) {
				if (numeros[k] != i) {
					division[j] += numeros[k];
					pos++;
				} else if(k==0){
					pos++;
				}else{
					pos++;
					break;
				}
			}
		}

		return division;

	}
}
