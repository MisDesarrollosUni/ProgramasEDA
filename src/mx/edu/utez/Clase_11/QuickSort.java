package mx.edu.utez.Clase_11;

public class QuickSort {

	/*public static void main(String[] args) {
		int array[] = {12, 9, 4, 99, 120};
		System.out.println("    Quick Sort\n");
		System.out.println("Valores antes de QuickSort:\n");
		imprimir(array);
		quick_srt(array, 0, array.length - 1);
		System.out.print("\nValores despues de QuickSort:\n\n");
		imprimir(array);

	}

	public static void imprimir(int array[]) {
		for (int i = 0; i < array.length; i++)
			System.out.print(array[i] + "  ");
	}

	public static void quick_srt(int array[], int low, int n) {
		int lo = low;
		int hi = n;
		if (lo >= n)
			return;
		int mid = array[(lo + hi) / 2];
		while (lo < hi) {
			while (lo < hi && array[lo] < mid)
				lo++;
			while (lo < hi && array[hi] > mid)
				hi--;
			if (lo < hi) {
				int T = array[lo];
				array[lo] = array[hi];
				array[hi] = T;
			}
		}
		if (hi < lo) {
			int T = hi;
			hi = lo;
			lo = T;
		}
		quick_srt(array, low, lo);
		quick_srt(array, lo == low ? lo + 1 : lo, n);
	}*/


	public static void main(String[] args) {
		int a[] = {1, 88, 5, 4, 3, 2, 85, 0, 12, 3, 7, 9, 8, 3, 100, 5, 7};
		int []b=quicksort(a, 0, a.length - 1);
		for (int i = 0; i < b.length ; i++) {
			System.out.print(b[i]+" ");
		}
	}

	static int partition(int[] a, int low, int hi) {
		int pivot = hi;
		int i = low;
		int j = hi;
		while (i < j) {
			if (a[i] <= a[pivot]) {
				i++;
			}
			if (a[i] > a[pivot]) {
				if ((a[i] > a[pivot]) && (a[j] <= a[pivot])) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
					i++;
				}
				if (a[j] > a[pivot]) {
					j--;
				}
			}
		}
		int temp = a[i];
		a[i] = a[pivot];
		a[pivot] = temp;
		return i;
	}

	static int[] quicksort(int[] a, int low, int hi) {
		if (low >= hi) {
			return a;
		}
		int split = partition(a, low, hi);
		quicksort(a, low, split - 1);
		quicksort(a, split + 1, hi);
		return a;
	}
}


