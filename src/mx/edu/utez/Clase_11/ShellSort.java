package mx.edu.utez.Clase_11;

public class ShellSort {
	public static int arr[] = {85, 69, 32, 2, 3};

	public static void main(String[] args) {
		System.out.println("Arreglo antes de Ordenar");
		printArray(arr);
		sort(arr);
		System.out.println("Arreglo después de Ordenar");
		printArray(arr);
	}

	static void printArray(int arr[]) {
		int n = arr.length;
		for (int i = 0; i < n; ++i)
			System.out.print(arr[i] + " ");
		System.out.println();
	}

	static int sort(int arr[]) {
		int n = arr.length;
		for (int gap = n / 2; gap > 0; gap /= 2) {
			for (int i = gap; i < n; i += 1) {
				int temp = arr[i];
				int j;
				for (j = i; j >= gap && arr[j - gap] > temp; j -= gap){
					arr[j] = arr[j - gap];
				}
				arr[j] = temp;
			}
		}
		return 0;
	}
}
