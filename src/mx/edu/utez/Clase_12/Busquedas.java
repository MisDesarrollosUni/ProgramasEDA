package mx.edu.utez.Clase_12;

public class Busquedas {
	public int busquedaSecuencial(int clave, int[] arreglo) {
		int pos = -1;
		for (int i = 0; i < arreglo.length; i++) {
			if (arreglo[i] == clave) {
				pos = i;
				break;
			}
		}
		return pos;
	}

	public int busquedaBinaria(int clave, int[] arreglo) {
		int pos = -1;
		arreglo = bubbleSort(arreglo);

		for (int i:arreglo) {
			System.out.print(i + " ");
		}

		int mitad = arreglo.length / 2;
		int elemento = arreglo[mitad];
		if (clave > elemento) {
			for (int i = mitad; i < arreglo.length; i++) {
				if (arreglo[i] == clave) {
					pos = i;
					break;
				}
			}
		} else if (clave < elemento) {
			for (int i = 0; i < mitad; i++) {
				if (arreglo[i] == clave) {
					pos = i;
					break;
				}
			}
		} else {
			pos = mitad;
		}
		return pos;
	}

	public int[] bubbleSort(int[] arreglo) {
		int aux = 0;
		for (int i = 0; i < arreglo.length; i++) {
			for (int j = i; j < arreglo.length - 1; j++) {
				if (!(arreglo[i] < arreglo[j + 1])) {
					aux = arreglo[i];
					arreglo[i] = arreglo[j + 1];
					arreglo[j + 1] = aux;
				}
			}
		}
		return arreglo;
	}
}
