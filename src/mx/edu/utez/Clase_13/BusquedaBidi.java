package mx.edu.utez.Clase_13;

import java.util.Scanner;

public class BusquedaBidi {
	private int tam = 4;
	private int[][] array = new int[tam][tam];
	private Scanner leer = new Scanner(System.in);

	public void menu() {
		System.out.println("\n" +
				"▒█▀▀█ ▒█░▒█ ▒█▀▀▀█ ▒█▀▀█ ▒█░▒█ ▒█▀▀▀ ▒█▀▀▄ ░█▀▀█ 　 ▒█▀▀█ ▀█▀ ▒█▄░▒█ ░█▀▀█ ▒█▀▀█ ▀█▀ ░█▀▀█ \n" +
				"▒█▀▀▄ ▒█░▒█ ░▀▀▀▄▄ ▒█░▒█ ▒█░▒█ ▒█▀▀▀ ▒█░▒█ ▒█▄▄█ 　 ▒█▀▀▄ ▒█░ ▒█▒█▒█ ▒█▄▄█ ▒█▄▄▀ ▒█░ ▒█▄▄█ \n" +
				"▒█▄▄█ ░▀▄▄▀ ▒█▄▄▄█ ░▀▀█▄ ░▀▄▄▀ ▒█▄▄▄ ▒█▄▄▀ ▒█░▒█ 　 ▒█▄▄█ ▄█▄ ▒█░░▀█ ▒█░▒█ ▒█░▒█ ▄█▄ ▒█░▒█");
		numerosRandom();
		imprimir();
		int buscar = buscarNumero();
		String resul = busquedaBinaria(buscar);
		if(resul != null){
			System.out.println("El número se encuentra en la poscición: "+resul);
		}else{
			System.out.println("El número no se encontro");
		}
		imprimir();

	}

	public String busquedaBinaria(int clave) {
		String pos = null;
		metodoBurbuja();
		int mitad = tam / 2;
		int elemento = array[mitad][mitad];
		if (clave > elemento) {
			for (int i = mitad; i <  array.length; i++) {
				for (int j = mitad; j <  array.length; j++) {
					if(array[i][j] == clave){
						pos = "["+i+"]["+j+"]";
					}
				}
			}
		} else if (clave < elemento) {
			for (int i = 0; i <  mitad; i++) {
				for (int j = 0; j <  mitad; j++) {
					if(array[i][j] == clave){
						pos = "["+i+"]["+j+"]";
					}
				}
			}
		} else {
			pos = "["+mitad+"]["+mitad+"]";
		}
		return pos;
	}

	public int buscarNumero() {
		boolean flag = false;
		int num = 0;
		do {
			try {
				System.out.print("Número a búscar: ");
				num = leer.nextInt();
				flag = true;
			} catch (Exception e) {
				System.out.println("\t*Solo número válidos");
				leer.next();
			}
		} while (!flag);
		return num;

	}

	public void numerosRandom() {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = (int) Math.ceil(Math.random() * 30);
			}
		}
	}

	public void imprimir() {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				System.out.print(array[i][j] + "  ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public void metodoBurbuja() {
		int aux = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				for (int k = 0; k < array.length; k++) {
					for (int l = 0; l < array.length; l++) {
						if (array[i][j] < array[k][l]) {
							aux = array[i][j];
							array[i][j] = array[k][l];
							array[k][l] = aux;
						}
					}
				}
			}
		}
	}
}
