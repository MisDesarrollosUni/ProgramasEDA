package mx.edu.utez.Clase_13;

import java.util.Scanner;

public class BusquedaBinaria {
	private int[] array = new int[20];
	private Scanner leer = new Scanner(System.in);

	public void menu() {
		System.out.println("\n" +
				"▒█▀▀█ ▒█░▒█ ▒█▀▀▀█ ▒█▀▀█ ▒█░▒█ ▒█▀▀▀ ▒█▀▀▄ ░█▀▀█ 　 ▒█▀▀█ ▀█▀ ▒█▄░▒█ ░█▀▀█ ▒█▀▀█ ▀█▀ ░█▀▀█ \n" +
				"▒█▀▀▄ ▒█░▒█ ░▀▀▀▄▄ ▒█░▒█ ▒█░▒█ ▒█▀▀▀ ▒█░▒█ ▒█▄▄█ 　 ▒█▀▀▄ ▒█░ ▒█▒█▒█ ▒█▄▄█ ▒█▄▄▀ ▒█░ ▒█▄▄█ \n" +
				"▒█▄▄█ ░▀▄▄▀ ▒█▄▄▄█ ░▀▀█▄ ░▀▄▄▀ ▒█▄▄▄ ▒█▄▄▀ ▒█░▒█ 　 ▒█▄▄█ ▄█▄ ▒█░░▀█ ▒█░▒█ ▒█░▒█ ▄█▄ ▒█░▒█");
		numerosRandom();
		imprimir();
		int buscar = setBusqueda();
		int resul = busquedaBinaria(buscar);
		if (resul != -1) {
			System.out.println("El número esta en la poscición: " + resul);
		} else {
			System.out.println("No se encuentro el número");
		}
		imprimir();
	}

	public int setBusqueda() {
		boolean flag = false;
		int num = 0;
		do {
			try {
				System.out.print("Número para búscar: ");
				num = leer.nextInt();
				flag = true;
			} catch (Exception e) {
				System.out.println("\t*Solo números válidos");
				leer.next();
			}
		} while (!flag);
		return num;
	}

	public void numerosRandom() {
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) Math.ceil(Math.random() * 50);
		}
	}

	public void imprimir() {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	public int busquedaBinaria(int clave) {
		int pos = -1;
		bubbleSort();
		int mitad = array.length / 2;
		int elemento = array[mitad];
		if (clave > elemento) {
			for (int i = mitad; i < array.length; i++) {
				if (array[i] == clave) {
					pos = i;
					break;
				}
			}
		} else if (clave < elemento) {
			for (int i = 0; i < mitad; i++) {
				if (array[i] == clave) {
					pos = i;
					break;
				}
			}
		} else {
			pos = mitad;
		}
		return pos;
	}

	public void bubbleSort() {
		int aux = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length - 1; j++) {
				if (!(array[i] < array[j + 1])) {
					aux = array[i];
					array[i] = array[j + 1];
					array[j + 1] = aux;
				}
			}
		}

	}
}
