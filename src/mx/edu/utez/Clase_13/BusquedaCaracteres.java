package mx.edu.utez.Clase_13;

public class BusquedaCaracteres {
	private char[] matriz = new char[10];
	private String buscarVocal = "AEIOU";

	public void menu() {
		System.out.println("\n" +
				"░█▀▀█ ░█─░█ ░█▀▀▀█ ░█▀▀█ ░█─░█ ░█▀▀▀ ░█▀▀▄ ─█▀▀█ 　 ░█▀▀▀█ ░█▀▀▀ ░█▀▀█ ░█─░█ ░█▀▀▀ ░█▄─░█ ░█▀▀█ ▀█▀ ─█▀▀█ ░█─── \n" +
				"░█▀▀▄ ░█─░█ ─▀▀▀▄▄ ░█─░█ ░█─░█ ░█▀▀▀ ░█─░█ ░█▄▄█ 　 ─▀▀▀▄▄ ░█▀▀▀ ░█─── ░█─░█ ░█▀▀▀ ░█░█░█ ░█─── ░█─ ░█▄▄█ ░█─── \n" +
				"░█▄▄█ ─▀▄▄▀ ░█▄▄▄█ ─▀▀█▄ ─▀▄▄▀ ░█▄▄▄ ░█▄▄▀ ░█─░█ 　 ░█▄▄▄█ ░█▄▄▄ ░█▄▄█ ─▀▄▄▀ ░█▄▄▄ ░█──▀█ ░█▄▄█ ▄█▄ ░█─░█ ░█▄▄█");
		llenar();
		imprimir();
		int resul = getCaracter();
		if (resul != -1) {
			System.out.println("El primer carácter está en la poscición: " + resul);
		} else {
			System.out.println("No hay ningúna vocal");
		}
	}

	public void llenar() {
		for (int i = 0; i < matriz.length; i++) {
			matriz[i] = (char) (Math.random() * (90 - 65 + 1) + 65);
		}
	}

	public void imprimir() {
		for (int i = 0; i < matriz.length; i++) {
			System.out.print(matriz[i] + " ");
		}
		System.out.println();
	}

	public int getCaracter() {
		int pos = -1;
		for (int i = 0; i < matriz.length; i++) {
			if (buscarVocal.indexOf(matriz[i]) != -1) {
				pos = i;
				break;
			}
		}
		return pos;
	}
}
