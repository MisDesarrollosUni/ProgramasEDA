package mx.edu.utez.Clase_13;

import java.util.Scanner;

public class BusquedaNombres {
	private String[] array = new String[20];
	private Scanner leer = new Scanner(System.in);

	public void menu() {
		leer.useDelimiter("\n");
		System.out.println("\n" +
				"▒█▀▀█ ▒█░▒█ ▒█▀▀▀█ ▒█▀▀█ ▒█░▒█ ▒█▀▀▀ ▒█▀▀▄ ░█▀▀█ 　 ▒█▀▀▀█ ▒█▀▀▀ ▒█▀▀█ ▒█░▒█ ▒█▀▀▀ ▒█▄░▒█ ▒█▀▀█ ▀█▀ ░█▀▀█ ▒█░░░ \n" +
				"▒█▀▀▄ ▒█░▒█ ░▀▀▀▄▄ ▒█░▒█ ▒█░▒█ ▒█▀▀▀ ▒█░▒█ ▒█▄▄█ 　 ░▀▀▀▄▄ ▒█▀▀▀ ▒█░░░ ▒█░▒█ ▒█▀▀▀ ▒█▒█▒█ ▒█░░░ ▒█░ ▒█▄▄█ ▒█░░░ \n" +
				"▒█▄▄█ ░▀▄▄▀ ▒█▄▄▄█ ░▀▀█▄ ░▀▄▄▀ ▒█▄▄▄ ▒█▄▄▀ ▒█░▒█ 　 ▒█▄▄▄█ ▒█▄▄▄ ▒█▄▄█ ░▀▄▄▀ ▒█▄▄▄ ▒█░░▀█ ▒█▄▄█ ▄█▄ ▒█░▒█ ▒█▄▄█");
		asignarNombres();
		imprimir();
		String nombre;
		System.out.print("Ingrese un nombre a búscar: ");
		nombre = leer.next();
		int resul = buscarNombre(nombre);
		if (resul != -1) {
			System.out.println("El nombre esta en la poscición: " + resul);
		} else {
			System.out.println("El nombre no se encontro");
		}
	}

	public int buscarNombre(String buscar) {
		int pos = -1;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(buscar)) {
				pos = i;
				break;
			}
		}
		return pos;
	}

	public void imprimir() {
		for (String nom : array) {
			System.out.print(nom + " | ");
		}
		System.out.println();
	}

	public void asignarNombres() {
		array[0] = "Hector";
		array[1] = "Sebastián";
		array[2] = "Alexis";
		array[3] = "Raúl";
		array[4] = "Gandy";
		array[5] = "Abraham";
		array[6] = "Gustavo";
		array[7] = "Omar";
		array[8] = "Jose";
		array[9] = "Manuel";
		array[10] = "Andrés";
		array[11] = "Walfred";
		array[12] = "Berenice";
		array[13] = "Valerie";
		array[14] = "Michelle";
		array[15] = "Guadalupe";
		array[16] = "Evelyn";
		array[17] = "Aldo";
		array[18] = "Daniel";
		array[19] = "Francisco";
	}
}
