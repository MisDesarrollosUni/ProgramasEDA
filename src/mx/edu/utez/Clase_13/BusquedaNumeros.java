package mx.edu.utez.Clase_13;

import java.util.Scanner;

public class BusquedaNumeros {
	private int[] array = new int[10];
	private Scanner leer = new Scanner(System.in);

	public void menu() {
		System.out.println("\n" +
				"░█▀▀█ ░█─░█ ░█▀▀▀█ ░█▀▀█ ░█─░█ ░█▀▀▀ ░█▀▀▄ ─█▀▀█ 　 ░█▀▀▀█ ░█▀▀▀ ░█▀▀█ ░█─░█ ░█▀▀▀ ░█▄─░█ ░█▀▀█ ▀█▀ ─█▀▀█ ░█─── \n" +
				"░█▀▀▄ ░█─░█ ─▀▀▀▄▄ ░█─░█ ░█─░█ ░█▀▀▀ ░█─░█ ░█▄▄█ 　 ─▀▀▀▄▄ ░█▀▀▀ ░█─── ░█─░█ ░█▀▀▀ ░█░█░█ ░█─── ░█─ ░█▄▄█ ░█─── \n" +
				"░█▄▄█ ─▀▄▄▀ ░█▄▄▄█ ─▀▀█▄ ─▀▄▄▀ ░█▄▄▄ ░█▄▄▀ ░█─░█ 　 ░█▄▄▄█ ░█▄▄▄ ░█▄▄█ ─▀▄▄▀ ░█▄▄▄ ░█──▀█ ░█▄▄█ ▄█▄ ░█─░█ ░█▄▄█");
		System.out.println("\tMENOR DE 1O NÚMEROS DADOS POR EL USUARIO");
		llenar();
		imprimir();
		int pos = getNumeroMenor();
		System.out.println("El número menor es: " + array[pos] + " en la poscición: " + pos);
	}

	public int getNumeroMenor() {
		int pos = array.length / 2;
		int numMenor = array[pos];
		for (int i = 0; i < array.length; i++) {
			if (array[i] < numMenor) {
				numMenor = array[i];
				pos = i;
			}
		}
		return pos;
	}

	public void imprimir() {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	public void llenar() {
		boolean flag = false;
		for (int i = 0; i < array.length; i++) {
			do {
				try {
					System.out.print("Número " + (i + 1) + ": ");
					array[i] = leer.nextInt();
					flag = true;
				} catch (Exception e) {
					System.out.println("\t*Solo números válidos");
					leer.next();
				}
			} while (!flag);
		}
	}
}
