package mx.edu.utez.Clase_13;

import java.util.Scanner;

public class MergeSortUsuario {
	private int[] array = new int[20];
	private Scanner leer = new Scanner(System.in);

	public void menu() {
		System.out.println("\n" +
				"▒█▀▄▀█ ▒█▀▀▀ ▒█▀▀█ ▒█▀▀█ ▒█▀▀▀ 　 ▒█▀▀▀█ ▒█▀▀▀█ ▒█▀▀█ ▀▀█▀▀ \n" +
				"▒█▒█▒█ ▒█▀▀▀ ▒█▄▄▀ ▒█░▄▄ ▒█▀▀▀ 　 ░▀▀▀▄▄ ▒█░░▒█ ▒█▄▄▀ ░▒█░░ \n" +
				"▒█░░▒█ ▒█▄▄▄ ▒█░▒█ ▒█▄▄█ ▒█▄▄▄ 　 ▒█▄▄▄█ ▒█▄▄▄█ ▒█░▒█ ░▒█░░");
		System.out.println("LLENADO DE ARREGLO");
		llenar();
		System.out.println("\nARRELO SIN ORDENAR");
		imprimir();
		mergeSort(array, array.length);
		System.out.println("ARRELO ORDENADO");
		imprimir();
	}

	public void llenar() {
		boolean flag = false;
		for (int i = 0; i < array.length; i++) {
			do {
				try {
					System.out.print("Número " + (i + 1) + ": ");
					array[i] = leer.nextInt();
					flag = true;
				} catch (Exception e) {
					System.out.println("\t*Solo números válidos");
					leer.next();
				}
			} while (!flag);
		}
	}

	public void imprimir() {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	public void mergeSort(int[] a, int n) {
		if (n < 2) {
			return;
		}
		int mid = n / 2;
		int[] l = new int[mid];
		int[] r = new int[n - mid];

		for (int i = 0; i < mid; i++) {
			l[i] = a[i];
		}
		for (int i = mid; i < n; i++) {
			r[i - mid] = a[i];
		}
		mergeSort(l, mid);
		mergeSort(r, n - mid);
		merge(a, l, r, mid, n - mid);
	}

	public void merge(int[] a, int[] l, int[] r, int left, int right) {
		int i = 0, j = 0, k = 0;
		while (i < left && j < right) {
			if (l[i] <= r[j]) {
				a[k++] = l[i++];
			} else {
				a[k++] = r[j++];
			}
		}
		while (i < left) {
			a[k++] = l[i++];
		}

		while (j < right) {
			a[k++] = r[j++];
		}
	}
}
