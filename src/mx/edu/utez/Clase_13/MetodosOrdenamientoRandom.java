package mx.edu.utez.Clase_13;

public class MetodosOrdenamientoRandom {

	private int[] array = new int[5000];

	public void menu() {
		System.out.println("\n" +
				"▀▀█▀▀ ▀█▀ ▒█▀▀▀ ▒█▀▄▀█ ▒█▀▀█ ▒█▀▀▀█ \n" +
				"░▒█░░ ▒█░ ▒█▀▀▀ ▒█▒█▒█ ▒█▄▄█ ▒█░░▒█ \n" +
				"░▒█░░ ▄█▄ ▒█▄▄▄ ▒█░░▒█ ▒█░░░ ▒█▄▄▄█");
		System.out.println("\tMILISEGUNDOS");
		numerosRandom();
		long inicioUno = System.currentTimeMillis();
		metodoBurbuja();
		long finUno = System.currentTimeMillis();
		System.out.println("MÉTODO BURBUJA: "+ (finUno - inicioUno));

		numerosRandom();
		inicioUno = System.currentTimeMillis();
		shell();
		finUno = System.currentTimeMillis();

		System.out.println("MÉTODO SHELL SORT: "+(finUno - inicioUno));

		numerosRandom();
		inicioUno = System.currentTimeMillis();
		sorting(array, 0, array.length-1);
		finUno = System.currentTimeMillis();
		System.out.println("MÉTODO QUICK SORT: "+(finUno - inicioUno));

	}

	public void numerosRandom() {
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) Math.ceil(Math.random() * 100);
		}
	}

	public void imprimir() {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println("\n");
	}

	public void metodoBurbuja() {
		int aux = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length - 1; j++) {
				if (!(array[i] > array[j + 1])) {
					aux = array[i];
					array[i] = array[j + 1];
					array[j + 1] = aux;
				}
			}
		}
	}

	public void shell() {
		int increment = array.length / 2;
		while (increment > 0) {
			for (int i = increment; i < array.length; i++) {
				int j = i;
				int temp = array[i];
				while (j >= increment && array[j - increment] < temp) {
					array[j] = array[j - increment];
					j = j - increment;
				}
				array[j] = temp;
			}
			if (increment == 2) {
				increment = 1;
			} else {
				increment *= (5.0 / 11);
			}
		}

	}

	public int partition(int arr[], int left, int right) {
		int pivot = arr[left];
		int i = left;
		for (int j = left + 1; j <= right; j++) {
			if (arr[j] > pivot) {
				i = i + 1;
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}

		int temp = arr[i];
		arr[i] = arr[left];
		arr[left] = temp;

		return i;

	}

	public void sorting(int arr[], int left, int right) {
		if (left < right) {
			int q = partition(arr, left, right);
			sorting(arr, left, q);
			sorting(arr, q + 1, right);
		}
	}

}
