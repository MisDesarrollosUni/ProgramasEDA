

package mx.edu.utez.Clase_13;
import java.util.Scanner;

public class OrdenamientoElementos {

    private Scanner leer = new Scanner(System.in);
    private int arrayNums[] = new int[10];
    private String arrayStrings[] = new String[10];

    public void menuElementos() {
        System.out.println("\n" +
                "▒█▀▀█ ▒█░▒█ ▒█▀▀█ ▒█▀▀█ ▒█░░░ ▒█▀▀▀ 　 ▒█▀▀▀█ ▒█▀▀▀█ ▒█▀▀█ ▀▀█▀▀ \n" +
                "▒█▀▀▄ ▒█░▒█ ▒█▀▀▄ ▒█▀▀▄ ▒█░░░ ▒█▀▀▀ 　 ░▀▀▀▄▄ ▒█░░▒█ ▒█▄▄▀ ░▒█░░ \n" +
                "▒█▄▄█ ░▀▄▄▀ ▒█▄▄█ ▒█▄▄█ ▒█▄▄█ ▒█▄▄▄ 　 ▒█▄▄▄█ ▒█▄▄▄█ ▒█░▒█ ░▒█░░");
        System.out.println("MENÚ ORDENAMIENTO DE ELEMENTOS DESCENDENTEMENTE");
        leerNumeros();
        ordenarImprimirNumeros();
    }

    public void leerNumeros() {
        boolean flag = false;
        for (int i = 0; i < arrayNums.length; i++) {
            do {
                System.out.print("NÚMERO " + (i + 1) + ": ");
                try {
                    arrayNums[i] = leer.nextInt();
                    flag = true;
                } catch (Exception e) {
                    System.out.println("\tSOLO NÚMEROS ENTEROS");
                    leer.next();
                }
            } while (!flag);
        }
    }

    public void ordenarImprimirNumeros() {
        int aux = 0;
        for (int i = 0; i < arrayNums.length; i++) {
            for (int j = i; j < arrayNums.length - 1; j++) {
                if (!(arrayNums[i] > arrayNums[j + 1])) {
                    aux = arrayNums[i];
                    arrayNums[i] = arrayNums[j + 1];
                    arrayNums[j + 1] = aux;
                }
            }
        }
        System.out.println("\tIMPRESION");
        for (int arrayNum : arrayNums) {
            System.out.print(arrayNum + "  ");
        }
    }

     /*
        int opc = 0;
        do {
            try {
                System.out.println("\n\nMENÚ ORDENAMIENTO DE ELEMENTOS DESCENDENTEMENTE");
                System.out.println("\t1.- NÚMEROS");
                System.out.println("\t2.- CARACTER(ES)");
                System.out.println("\t3.- SALIR");
                System.out.print("ELIGA: ");
                opc = leer.nextInt();
                switch (opc) {
                    case 1:
                        leerNumeros();
                        ordenarImprimirNumeros();
                        break;
                    case 2:
                        leerStrings();
                        ordenarImprimirString();
                        break;
                    case 3:
                        opc = 3;
                        System.out.println("\tSALIENDO...");
                        break;
                    default:
                        System.out.println("NO EXISTE ESA OPCIÓN");
                }
            } catch (Exception e) {
                System.out.println("\tSOLO NÚMEROS PARA ELEGIR");
                leer.next();
            }

        } while (opc != 3);



    public void leerStrings() {
        leer.useDelimiter("\n");
        boolean flag = false;
        for (int i = 0; i < arrayStrings.length; i++) {
            do {
                System.out.print("CADENA " + (i + 1) + ": ");
                try {
                    arrayStrings[i] = leer.next();
                    if(arrayStrings[i].length()!=0){
                        flag = true;
                    }else{
                        System.out.println("\tNO DEBE ESTAR VACÍO");
                    }
                    
                } catch (Exception e) {
                    System.out.println("\tSOLO CADENAS");
                    leer.next();
                }
            } while (!flag);
        }
    }

    public void ordenarImprimirString() {
        String aux = "";
        for (int i = 0; i < arrayStrings.length; i++) {
            for (int j = i; j < arrayStrings.length - 1; j++) {
                if (arrayStrings[i].compareToIgnoreCase(arrayStrings[j + 1]) < 0) {
                    aux = arrayStrings[i];
                    arrayStrings[i] = arrayStrings[j + 1];
                    arrayStrings[j + 1] = aux;
                }
            }
        }
        System.out.println("\tIMPRESION");
        for (String arrayStrings : arrayStrings) {
            System.out.print(arrayStrings + " | ");
        }
    }
*/
}

