package mx.edu.utez.Clase_13;

public class ShellSortRandom {
	private int[] arregloNumeros = new int[30];

	public void menu(){
		System.out.println("\n" +
				"░█▀▀▀█ ░█─░█ ░█▀▀▀ ░█─── ░█─── 　 ░█▀▀▀█ ░█▀▀▀█ ░█▀▀█ ▀▀█▀▀ \n" +
				"─▀▀▀▄▄ ░█▀▀█ ░█▀▀▀ ░█─── ░█─── 　 ─▀▀▀▄▄ ░█──░█ ░█▄▄▀ ─░█── \n" +
				"░█▄▄▄█ ░█─░█ ░█▄▄▄ ░█▄▄█ ░█▄▄█ 　 ░█▄▄▄█ ░█▄▄▄█ ░█─░█ ─░█──");
		generarNumerosAleatorios();
		imprimirArreglo();
		System.out.println("\n");
		ordenacionShell();
		imprimirArreglo();
	}

	public void generarNumerosAleatorios(){
		for (int i = 0; i <arregloNumeros.length ; i++) {
			arregloNumeros[i] = (int)Math.ceil(Math.random()*50);
		}
	}

	public  void ordenacionShell() {
		int N = arregloNumeros.length;
		int incremento = N;
		do {
			incremento = incremento / 2;
			for (int k = 0; k < incremento; k++) {
				for (int i = incremento + k; i < N; i += incremento) {
					int j = i;
					while (j - incremento >= 0 && arregloNumeros[j] < arregloNumeros[j - incremento]) {
						int tmp = arregloNumeros[j];
						arregloNumeros[j] = arregloNumeros[j - incremento];
						arregloNumeros[j - incremento] = tmp;
						j -= incremento;
					}
				}
			}
		} while (incremento > 1);
	}

	public void imprimirArreglo(){
		for (int i = 0; i < arregloNumeros.length ; i++) {
			System.out.print(arregloNumeros[i]+" ");
		}
	}
}
