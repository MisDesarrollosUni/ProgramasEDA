package mx.edu.utez.Clase_14;

public class BatallaNaval {
	private String tablero[][] = new String[5][5];
	private String tableroAux[][] = new String[5][5];
	private int contador = 0;

	public int getContador() {
		return contador;
	}

	public BatallaNaval() {

	}

	public BatallaNaval(String[][] tablero, String[][] tableroAux) {
		this.tablero = tablero;
		this.tableroAux = tableroAux;
	}

	public void imprimirTablero() {
		if (contador == 5) {
			for (int i = 0; i < tablero.length; i++) {
				for (int j = 0; j < tablero[i].length; j++) {
					if (tablero[i][j].equals("-")) {
						tablero[i][j] = "A";
					}
				}
			}
		}

		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				System.out.print(tablero[i][j] + "  ");
			}
			System.out.println();
		}
	}

	public void imprimirTableroAux() {
		for (int i = 0; i < tableroAux.length; i++) {
			for (int j = 0; j < tableroAux[i].length; j++) {
				System.out.print(tableroAux[i][j] + "  ");
			}
			System.out.println();
		}
	}

	public void generarTablero() {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				tablero[i][j] = "-";
				tableroAux[i][j] = "-";
			}
		}

		for (int i = 0; i < tablero.length; i++) {
			int random = (int) Math.ceil(Math.random() * tablero[i].length - 1);
			tablero[i][random] = "*";
		}
	}

	public void tirar(int posI, int posJ) {
		if (tablero[posI][posJ].equals("*")) {
			tablero[posI][posJ] = "H";
			tableroAux[posI][posJ] = "H";
			contador++;
		} else {
			tablero[posI][posJ] = "A";
			tableroAux[posI][posJ] = "A";
		}
	}
}
