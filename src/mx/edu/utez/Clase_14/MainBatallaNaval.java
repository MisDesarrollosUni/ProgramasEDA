package mx.edu.utez.Clase_14;

import java.util.Scanner;

public class MainBatallaNaval {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		BatallaNaval a = new BatallaNaval();
		a.generarTablero();
		boolean b = true;
		do {
			System.out.println("Teclea una posición para i:");
			int i = sc.nextInt();
			System.out.println("Teclea una posición para j:");
			int j = sc.nextInt();
			if ((i < 5) && (j < 5)) {
				a.tirar(i, j);
				a.imprimirTableroAux();
				if (a.getContador() == 5) {
					b = true;
					break;
				}
			} else {
				System.out.println("Teclea posiciones validas");
			}
		} while (b);
		System.out.println();
		System.out.println("Ganaste!");
		a.imprimirTablero();
	}

}
