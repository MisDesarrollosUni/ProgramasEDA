package mx.edu.utez.Clase_14;

import java.util.Scanner;

public class SerpientesyEscaleras {
	private final String CARACTER = "*";
	private Scanner leer = new Scanner(System.in);
	private String[] mapaNumeros = new String[25];
	private int poscicionJugador = -1, control = 1;

	public void llenarTablero() {
		for (int i = 0, j = 1; i < mapaNumeros.length && j <= mapaNumeros.length; i++, j++) {
			mapaNumeros[i] = "" + j;
			if (j == 5 || j == 13 || j == 22)
				mapaNumeros[i] += "+";
			if (j == 9 || j == 19 || j == 24)
				mapaNumeros[i] += "-";
		}
	}

	public void iniciar() {
		llenarTablero();
		int opc = 0;
		System.out.println("▒█▀▀▀█ ▒█▀▀▀ ▒█▀▀█ ▒█▀▀█ ▀█▀ ▒█▀▀▀ ▒█▄░▒█ ▀▀█▀▀ ▒█▀▀▀ 　 ▒█░░▒█ 　 ▒█▀▀▀ ▒█▀▀▀█ ▒█▀▀█ ░█▀▀█ ▒█░░░ ▒█▀▀▀ ▒█▀▀█ ░█▀▀█ ▒█▀▀▀█ \n"
				+ "░▀▀▀▄▄ ▒█▀▀▀ ▒█▄▄▀ ▒█▄▄█ ▒█░ ▒█▀▀▀ ▒█▒█▒█ ░▒█░░ ▒█▀▀▀ 　 ▒█▄▄▄█ 　 ▒█▀▀▀ ░▀▀▀▄▄ ▒█░░░ ▒█▄▄█ ▒█░░░ ▒█▀▀▀ ▒█▄▄▀ ▒█▄▄█ ░▀▀▀▄▄ \n"
				+ "▒█▄▄▄█ ▒█▄▄▄ ▒█░▒█ ▒█░░░ ▄█▄ ▒█▄▄▄ ▒█░░▀█ ░▒█░░ ▒█▄▄▄ 　 ░░▒█░░ 　 ▒█▄▄▄ ▒█▄▄▄█ ▒█▄▄█ ▒█░▒█ ▒█▄▄█ ▒█▄▄▄ ▒█░▒█ ▒█░▒█ ▒█▄▄▄█");
		do {
			control = 1;
			imprimirTablero();
			System.out.println();
			System.out.println("\t(1). LANZAR DADO\n\t(2). SALIR DEL JUEGO");
			System.out.print("Elegir: ");
			opc = validarNumero();
			switch (opc) {
				case 1:
					int dado = (int) Math.ceil(Math.random() * 6);
					System.out.println("\tDADO CAYÓ: " + dado);
					poscicionJugador += dado;
					if (poscicionJugador >= mapaNumeros.length) {
						opc = 2;
						break;
					}
					if (mapaNumeros[poscicionJugador].indexOf('+') != -1) {
						poscicionJugador += 3;
						System.out.println("\tAVANZASTE 3 CASILLAS CAISTE EN +");
					} else if (mapaNumeros[poscicionJugador].indexOf('-') != -1) {
						poscicionJugador -= 1;
						System.out.println("\tRETROCEDISTE 1 CASILLA CAISTE EN -");
					}
					System.out.println("\tESTAS EN EL NÚMERO: " + (poscicionJugador + 1));
					break;
				case 2:
					opc = 2;
					System.out.println("\tSALISTE DEL JUEGO ");
					break;
				default:
					System.out.println("\tNO EXISTE ESA OPCIÓN");
			}
			System.out.println("--------------------------------------------------------");
		} while (poscicionJugador < mapaNumeros.length && opc != 2);

		if (poscicionJugador >= mapaNumeros.length - 1) {
			System.out.println("▒█▀▀█ ░█▀▀█ ▒█▄░▒█ ░█▀▀█ ▒█▀▀▀█ ▀▀█▀▀ ▒█▀▀▀ █ \n"
					+ "▒█░▄▄ ▒█▄▄█ ▒█▒█▒█ ▒█▄▄█ ░▀▀▀▄▄ ░▒█░░ ▒█▀▀▀ ▀ \n"
					+ "▒█▄▄█ ▒█░▒█ ▒█░░▀█ ▒█░▒█ ▒█▄▄▄█ ░▒█░░ ▒█▄▄▄ ▄");
		}
	}


	public int validarNumero() {
		boolean flag = false;
		int random = 0;
		do {
			try {
				random = leer.nextInt();
				flag = true;
			} catch (Exception e) {
				System.out.println("\tSOLO NÚMEROS VÁLIDOS");
				System.out.print(": ");
				leer.next();
			}
		} while (!flag);
		return random;
	}

	public void imprimirTablero() {
		primeraLinea(0, 4);
		segundaLinea(9, 5);
		primeraLinea(10, 14);
		segundaLinea(19, 15);
		primeraLinea(20, 24);
	}

	public void primeraLinea(int inicio, int limite) {
		if (control == 1) {
			System.out.print("SALIDA -->");
		} else {
			System.out.print("          ");
		}
		control++;
		for (int i = inicio; i <= limite; i++) {
			if (poscicionJugador == i) {
				System.out.print(CARACTER + "  ");
			} else if (mapaNumeros[i].length() == 1) {
				System.out.print(mapaNumeros[i] + "   ");
			} else {
				System.out.print(mapaNumeros[i] + "  ");
			}

		}
		System.out.println();
	}

	public void segundaLinea(int inicio, int limite) {
		System.out.print("          ");
		for (int i = inicio; i >= limite; i--) {
			if (poscicionJugador == i) {
				System.out.print(CARACTER + "  ");
			} else if (mapaNumeros[i].length() == 1) {
				System.out.print(mapaNumeros[i] + "   ");
			} else {
				System.out.print(mapaNumeros[i] + "  ");
			}
		}
		System.out.println();
	}
}
