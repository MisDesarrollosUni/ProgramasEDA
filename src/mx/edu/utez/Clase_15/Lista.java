package mx.edu.utez.Clase_15;

public class Lista {
	NodoLista first;
	NodoLista last;

	public Lista() {
		first = null;
		last = null;
	}

	//LA LISTA ESTA VACIA O NO
	public boolean isEmpty() {
		if (this.first == null) {
			return true;
		} else {
			return false;
		}
	}

	public void add(String dato) {
		if (isEmpty()) { //SI ES EL PRIMERO DE LA LISTA EL PRIMERO ES EL ULTIMO Y EL PRIMERO
			first = new NodoLista(dato);
			last = first;
		} else {
			NodoLista aux = last; //SE VAN AGREGANDO, LOS NUEVOS VAN SIENDO LOS ULTIMOS
			NodoLista nuevo = new NodoLista(dato);
			aux.setNext(nuevo);
			last = nuevo;
		}
	}

	public void print() {
		NodoLista next = first;
		while (next != null) {
			System.out.print(next.getDato() + " --> ");
			next = next.getNext();
		}
	}

	public void remove() {
		if (!isEmpty()) {
			NodoLista aux = first.getNext();
			first = aux;
		}
	}

	public String get(int index) {
		int cont = 0;
		String data = "";
		NodoLista next = first;
		if (!isEmpty() && index < size()) {
			while (cont < index && next != null && cont < size()) {
				cont++;
				next = next.getNext();
			}
			data = next.getDato();
		}
		return data;
	}

	public int size() {
		int cont = 0;
		if (!isEmpty()) {
			NodoLista next = first;
			while (next != null) {
				cont++;
				next = next.getNext();
			}
		}
		return cont;
	}

	public void add(String data, int index) {
		NodoLista prev = first, next = first;
		int cont = 0;
		int size = size();
		if (!isEmpty() && index <= size && index >= 0) {
			while (next != null && cont != index) {
				cont++;
				prev = next;
				next = next.getNext();
			}
			NodoLista nuevo = new NodoLista(data);
			if (index != 0) {
				prev.setNext(nuevo);
			} else {
				first = nuevo;
			}
			if (index == size) {
				last = nuevo;
			}
			nuevo.setNext(next);
		}
	}

	public void remove(int index) {
		int cont = 0;
		if (!isEmpty() && index < size() && index >= 0) {
			NodoLista prev = first, next = first;
			if (index == 0) {
				remove();
			} else {
				while (next != null && cont != index) {
					cont++;
					prev = next;
					next = next.getNext();
				}
				if (next.getNext() != null) {
					next = next.getNext();
					prev.setNext(next);

				} else {
					prev.setNext(null);
					last = prev;
				}
			}
		}
	}

}
