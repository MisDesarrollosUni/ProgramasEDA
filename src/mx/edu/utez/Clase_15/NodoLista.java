package mx.edu.utez.Clase_15;

public class NodoLista {
	private String dato;
	private NodoLista next;


	public NodoLista(String dato) {
		this.dato = dato;
		this.next = null; //ES IMPORTANTE PORQUE SE SABE QUE ULTIMO DATO ES NULL
	}

	public String getDato() {
		return dato;
	}

	public void setDato(String dato) {
		this.dato = dato;
	}

	public NodoLista getNext() {
		return next;
	}

	public void setNext(NodoLista next) {
		this.next = next;
	}
}
