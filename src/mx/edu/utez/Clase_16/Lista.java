package mx.edu.utez.Clase_16;

public class Lista {
	NodoLista first, last;

	public Lista() {
		this.first = null;
		this.last = null;
	}

	//	METODO SI ESTA O NO VACIA LA LISTA
	public boolean isEmpty() {
		return this.first == null;
	}

	//	METODO ADD(INDEX)
	public void add(String data, int index) {
		int cont = 0;
		if (!isEmpty() && index <= size() && index >= 0) {
			NodoLista prev = first, next = first;
			while (next != null && cont != index) {
				cont++;
				prev = next;
				next = next.getNext();
			}
			NodoLista nuevo = new NodoLista(data);
			if (index == size()) {
				add(data);
			} else if (index != 0) {
				prev.setNext(nuevo);
				nuevo.setPrev(prev);
				nuevo.setNext(next);
				next.setPrev(nuevo);
			} else {
				first = nuevo;
				first.setNext(prev);
				prev.setPrev(nuevo);
			}
		}
	}

	//	METODO AGREGAR DATO
	public void add(String data) {
		NodoLista next = first;
		if (isEmpty()) {
			NodoLista nuevo = new NodoLista(data);
			first = nuevo;
			last = nuevo;
		} else {
			while (next.getNext() != null) {
				next = next.getNext();
			}
			NodoLista nuevo = new NodoLista(data);
			last = nuevo;
			nuevo.setPrev(next);
			next.setNext(nuevo);
		}
	}

	//	METODO IMPRIMIR NORMAL
	public void normalPrint() {
		if (!isEmpty()) {
			NodoLista next = first;
			while (next != null) {
				System.out.print(next.getData() + " ---> ");
				next = next.getNext();
			}
		}
	}

	//	METODO IMPRIMIR INVERSO
	public void reversePrint() {
		if (!isEmpty()) {
			NodoLista prev = last;
			while (prev != null) {
				System.out.print(" <--- " + prev.getData());
				prev = prev.getPrev();
			}
		}
	}

	//	METODO GET
	public String get(int index) {
		int cont = 0;
		String data = "";
		if (!isEmpty() && index >= 0 && index <= size()) {
			NodoLista next = first;
			while (next != null && index != cont) {
				cont++;
				next = next.getNext();
			}
			data = next.getData();
		}
		return data;
	}


	//	METODO SIZE
	public int size() {
		int cont = 0;
		if (!isEmpty()) {
			NodoLista next = first;
			while (next != null) {
				cont++;
				next = next.getNext();
			}
		}
		return cont;
	}

//	METODO REMOVE
	public void remove(){
		if(!isEmpty()){
			NodoLista aux = first.getNext();
			first = aux;
			first.setPrev(null);
		}
	}

//	METODO REMOVE (INDEX)
	public void remove(int index){
		int cont = 0;
		if(!isEmpty() && index<size() && index>=0){
			NodoLista prev = first, next = first;
			if (index == 0){
				remove();
			}else{
				while (next!= null && cont!=index){
					cont++;
					prev = next;
					next = next.getNext();
				}
				if(next.getNext() != null){
					next = next.getNext();
					prev.setNext(next);
					next.setPrev(prev);
				}else{
					prev.setNext(null);
					last = prev;
				}
			}
		}
	}


}

