package mx.edu.utez.Clase_16;

public class MainListaDoblementeEnlazada {
	public static void main(String[] args) {
		Lista lista = new Lista();
		lista.add("1");
		lista.add("2");
		lista.add("3");
		lista.add("4");
		lista.add("5");
		lista.normalPrint();
		System.out.println("\n---------------------------------");
		System.out.println(lista.get(2));
		lista.remove();
		lista.remove();
		lista.reversePrint();
		System.out.println("\n---------------------------------");
		lista.add("2", 0);
		lista.add("3.5", 2);
		lista.add("6", 5);
		lista.normalPrint();
		System.out.println("\n---------------------------------");
		lista.remove(1);
		lista.remove(3);
		lista.reversePrint();
		System.out.println("\n---------------------------------");
		System.out.println("\nEl tamaño de la lista es de: " + lista.size());







	}
}
