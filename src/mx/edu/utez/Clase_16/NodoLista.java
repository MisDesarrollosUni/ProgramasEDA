package mx.edu.utez.Clase_16;

public class NodoLista {
	String data;
	NodoLista next;
	NodoLista prev;


	public NodoLista(String data) {
		this.data = data;
		this.next = null;
		this.prev = null;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public NodoLista getNext() {
		return next;
	}

	public void setNext(NodoLista next) {
		this.next = next;
	}

	public NodoLista getPrev() {
		return prev;
	}

	public void setPrev(NodoLista prev) {
		this.prev = prev;
	}
}
