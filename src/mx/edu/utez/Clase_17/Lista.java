package mx.edu.utez.Clase_17;

public class Lista {
	NodoLista first;
	NodoLista last;

	public Lista() {
		this.first = null;
		this.last = null;
	}

	public boolean isEmpty() {
		return this.first == null;
	}

	public void print() {
		if (!isEmpty()) {
			NodoLista next = first;
			while (next.getNext() != first) {
				System.out.print(next.getData() + " -> ");
				next = next.getNext();
			}
			System.out.println(next.getData()); //IMPRIME EL ULTIMO ELEMENTO ANTES DEL FIRST
			System.out.println("CIRCULAR SIG: " + next.getNext().getData()); //CHECAMOS SI EN VERDAD DA LA VUELTA Y LLAMAMOS A LA SIGUIENTE POS.

		}
	}

	public int size() {
		int cont = 0;
		if (!isEmpty()) {
			NodoLista next = first;
			while (next.getNext() != first) {
				cont++;
				next = next.getNext();
			}
			cont++;
		}
		return cont;
	}

	public String get(int index) {
		String data = "";
		if (!isEmpty() && index >= 0 && index < size()) {
			int cont = 0;
			NodoLista next = first;
			while (next.getNext() != first && index != cont) {
				cont++;
				next = next.getNext();
			}
			data = next.getData();
		}
		return data;
	}

	public void add(String data) {
		if (isEmpty()) {
			first = new NodoLista(data);
			last = first;
			last.setNext(first);
		} else {
			NodoLista nuevo = new NodoLista(data);
			last.setNext(nuevo);
			last = nuevo;
			last.setNext(first);
		}
	}

	public void remove() {
		if (!isEmpty()) {
			NodoLista aux = first.getNext();
			first = aux;
			last.setNext(first);
		}
	}

	// METODO DELETE(INDEX), ADD(INDEX)

	public void remove(int index) {
		if (!isEmpty() && index < size() && index >= 0) {
			NodoLista prev = first, next = first;
			int cont = 0;
			if (index == 0) {
				remove();
			} else {
				while (next.getNext() != first && index != cont) {
					cont++;
					prev = next;
					next = next.getNext();
				}
				next = next.getNext();
				if (index!=size()) {
					prev.setNext(next);
				} else {
					prev.setNext(first);
				}
			}
		}
	}

	public void add(String data, int index) {
		if (!isEmpty() && index <= size() && index >= 0) {
			NodoLista prev = first, next = first;
			int cont = 0;
			while (next.getNext() != first && index != cont) {
				cont++;
				prev = next;
				next = next.getNext();
			}
			NodoLista nuevo = new NodoLista(data);
			if (index == size()) {
				add(data);
			} else if (index != 0) {
				prev.setNext(nuevo);
				nuevo.setNext(next);
			} else {
				first = nuevo;
				nuevo.setNext(prev);
				last.setNext(first);
			}


		}
	}

}
