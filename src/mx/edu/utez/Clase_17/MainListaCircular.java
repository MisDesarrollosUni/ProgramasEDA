package mx.edu.utez.Clase_17;

public class MainListaCircular {
	public static void main(String[] args) {
		Lista lista = new Lista();

		lista.add("1");
		lista.add("2");
		lista.add("3");
		lista.add("4");
		lista.add("5");
		lista.print();

		System.out.println("--------------------------------------");

		System.out.println(lista.get(3));
		System.out.println(lista.get(5));
		lista.remove();
		lista.remove();
		lista.print();

		System.out.println("--------------------------------------");

		lista.add("2",0);
		lista.add("3.5",2);
		lista.add("6",5);
		lista.print();

		System.out.println("--------------------------------------");

		lista.remove(2);
		lista.remove(4);
		lista.print();

		System.out.println("--------------------------------------");

		System.out.println("TAMAÑO: "+lista.size());


	}
}
