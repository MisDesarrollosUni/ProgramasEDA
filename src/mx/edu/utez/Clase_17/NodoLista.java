package mx.edu.utez.Clase_17;

public class NodoLista {
	private String data;
	private NodoLista next;

	public NodoLista(String data) {
		this.data = data;
		this.next = null;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public NodoLista getNext() {
		return next;
	}

	public void setNext(NodoLista next) {
		this.next = next;
	}
}
