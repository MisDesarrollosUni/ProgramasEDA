package mx.edu.utez.Clase_18;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JugarGato {
	private List< Character> tablaGato = new ArrayList<>();
	private Scanner leer = new Scanner(System.in);
	private char X = 'X';
	private char O = 'O';
	private char jugador;
	private char compu;

	public void jugarGato() {
		System.out.println("▒█▀▀█ ░█▀▀█ ▀▀█▀▀ ▒█▀▀▀█ \n" +
				"▒█░▄▄ ▒█▄▄█ ░▒█░░ ▒█░░▒█ \n" +
				"▒█▄▄█ ▒█░▒█ ░▒█░░ ▒█▄▄▄█");
		int coordenada = 0, turnos = 0, ganador = 0;
		llenarTablero();
		elegirCaracter();
		while (turnos < 9) {
			do {
				imprimirTabla();
				System.out.print("Ingrese una número: ");
				coordenada = leer.nextInt();
				coordenada--;
				if (noVacio(coordenada))
					System.out.println("Error ingrese otra");
			} while (noVacio(coordenada));
			anchoo(coordenada, jugador);
			if (turnos != 8)
				turnoCompu();
			ganador = validarGanador();
			switch(ganador){
				case 1:
					imprimirTabla();
					System.out.println("░█▀▀█ ─█▀▀█ ░█▄─░█ ░█▀▀▀█ 　 ───░█ ░█─░█ ░█▀▀█ ─█▀▀█ ░█▀▀▄ ░█▀▀▀█ ░█▀▀█ \n" +
							"░█─▄▄ ░█▄▄█ ░█░█░█ ░█──░█ 　 ─▄─░█ ░█─░█ ░█─▄▄ ░█▄▄█ ░█─░█ ░█──░█ ░█▄▄▀ \n" +
							"░█▄▄█ ░█─░█ ░█──▀█ ░█▄▄▄█ 　 ░█▄▄█ ─▀▄▄▀ ░█▄▄█ ░█─░█ ░█▄▄▀ ░█▄▄▄█ ░█─░█");
					return;
				case 2:
					imprimirTabla();
					System.out.println("░█▀▀█ ─█▀▀█ ░█▄─░█ ░█▀▀▀█ 　 ░█▀▀█ ░█▀▀█ \n" +
							"░█─▄▄ ░█▄▄█ ░█░█░█ ░█──░█ 　 ░█▄▄█ ░█─── \n" +
							"░█▄▄█ ░█─░█ ░█──▀█ ░█▄▄▄█ 　 ░█─── ░█▄▄█");
					return;
			}
			turnos += 2;
		}
		System.out.println("░█▀▀▀ ░█▀▄▀█ ░█▀▀█ ─█▀▀█ ▀▀█▀▀ ░█▀▀▀ \n" +
				"░█▀▀▀ ░█░█░█ ░█▄▄█ ░█▄▄█ ─░█── ░█▀▀▀ \n" +
				"░█▄▄▄ ░█──░█ ░█─── ░█─░█ ─░█── ░█▄▄▄");
	}

	// Elige el jugador
	public void elegirCaracter() {
		boolean flag = false;
		int carac;
		do {
			try {
				System.out.println("¿Qué carácter? \n\t 1. X     2.  O: ");
				System.out.print("Elige: ");
				carac = leer.nextInt();
				if (carac == 1) {
					jugador = X;
					compu = O;
					flag = true;
				} else if (carac == 2) {
					jugador = O;
					compu = X;
					flag = true;
				}
			} catch (Exception e) {
				leer.next();
			}
		} while (!flag);

	}

	// Valida si alguien de los jugadores ha ganado
	public int validarGanador() {
		int aux = 0;
		aux = validarD();
		if (aux != 0)  return aux;
		aux = validarH();
		if (aux != 0) return aux;
		aux = validarV();
		if (aux != 0) return aux;
		return 0;
	}

	// Validamos ganador en horizontales
	public int validarH() {
		if (tablaGato.get(0) == jugador && tablaGato.get(1) == jugador && tablaGato.get(2) == jugador) {
			return 1;
		}
		if (tablaGato.get(6) == jugador && tablaGato.get(7) == jugador && tablaGato.get(8) == jugador) {
			return 1;
		}
		if (tablaGato.get(3) == jugador && tablaGato.get(4) == jugador && tablaGato.get(5) == jugador) {
			return 1;
		}
		if (tablaGato.get(0) == compu && tablaGato.get(1) == compu && tablaGato.get(2) == compu) {
			return 2;
		}
		if (tablaGato.get(3) == compu && tablaGato.get(4) == compu && tablaGato.get(5) == compu) {
			return 2;
		}

		if (tablaGato.get(6) == compu && tablaGato.get(7) == compu && tablaGato.get(8) == compu) {
			return 2;
		}
		return 0;
	}

	// Validamos ganador en verticales
	public int validarV() {
		if (tablaGato.get(0) == jugador && tablaGato.get(3) == jugador && tablaGato.get(6) == jugador) {
			return 1;
		}
		if (tablaGato.get(1) == jugador && tablaGato.get(4) == jugador && tablaGato.get(7) == jugador) {
			return 1;
		}
		if (tablaGato.get(0) == compu && tablaGato.get(3) == compu && tablaGato.get(6) == compu) {
			return 2;
		}
		if (tablaGato.get(2) == jugador && tablaGato.get(5) == jugador && tablaGato.get(8) == jugador) {
			return 1;
		}
		if (tablaGato.get(1) == compu && tablaGato.get(4) == compu && tablaGato.get(7) == compu) {
			return 2;
		}
		if (tablaGato.get(2) == compu && tablaGato.get(5) == compu && tablaGato.get(8) == compu) {
			return 2;
		}
		return 0;
	}

	// Validamos ganador en diagonales
	public int validarD() {
		if (tablaGato.get(0) == jugador && tablaGato.get(4) == jugador && tablaGato.get(8) == jugador) {
			return 1;
		}
		if (tablaGato.get(2) == jugador && tablaGato.get(4) == jugador && tablaGato.get(6) == jugador) {
			return 1;
		}
		if (tablaGato.get(0) == compu && tablaGato.get(4) == compu && tablaGato.get(8) == compu) {
			return 2;
		}
		if (tablaGato.get(2) == compu && tablaGato.get(4) == compu && tablaGato.get(6) == compu) {
			return 2;
		}
		return 0;
	}

	public void anchoo(int index, char car){
		tablaGato.set(index, car) ;
	}

	// Verifica si esta vacio o no
	public boolean noVacio(int index) {
		if (index < 0 || index > tablaGato.size()) return false;
		return tablaGato.get(index) == X || tablaGato.get(index) == O;
	}

	// Tira la computadora
	public void turnoCompu() {
		int random;
		do {
			random = (int) (Math.random() * tablaGato.size());
		} while (noVacio(random));
		tablaGato.set(random, compu);
	}

	// Se llena todo el arreglo con un caracter y asi no este vacío
	public void llenarTablero() {
		for (int i = 0; i < 9; i++) {
			tablaGato.add('-');
		}
	}

	// Se imprime el tablero dependiendo si tiene o no un carácter lo imprime
	public void imprimirTabla(){
		int i=0 ;
		int lim = 3 ;
		for(; i<lim; i++){
			System.out.print( tablaGato.get(i) == X || tablaGato.get(i) == O ? "\t"+"[ "+tablaGato.get(i)+" ]" : "\t"+"[ "+(i+1)+" ]" );
		}
		System.out.println("\n");
		lim+=3;
		for(; i<lim; i++){
			System.out.print( tablaGato.get(i) == X || tablaGato.get(i) == O ? "\t"+"[ "+tablaGato.get(i)+" ]" :"\t"+ "[ "+(i+1)+" ]" );
		}
		lim+=3;
		System.out.println("\n");
		for(; i<lim; i++){
			System.out.print(tablaGato.get(i) == X || tablaGato.get(i) == O ? "\t"+"[ "+tablaGato.get(i)+" ]" : "\t"+ "[ "+(i+1)+" ]" );
		}
		System.out.println("\n");
	}

}
