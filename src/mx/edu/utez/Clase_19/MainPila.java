package mx.edu.utez.Clase_19;

public class MainPila {
	public static void main(String[] args) {
		Pila pila = new Pila();
		pila.push("1"); //bottom
		pila.push("2");
		pila.push("3");
		pila.push("4");
		pila.push("5"); // top
		pila.print();
		System.out.println("-------------------------------");
		pila.pop();
		pila.print();
		System.out.println("-------------------------------");

		System.out.println(pila.peek());
		pila.pop();
		pila.pop();
		System.out.println("-------------------------------");

		pila.print();
	}
}
