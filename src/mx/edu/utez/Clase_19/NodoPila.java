package mx.edu.utez.Clase_19;

public class NodoPila {
	String data;
	NodoPila next;

	public NodoPila(String data) {
		this.data = data;
		this.next = null;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public NodoPila getNext() {
		return next;
	}

	public void setNext(NodoPila next) {
		this.next = next;
	}
}
