package mx.edu.utez.Clase_19;

public class Pila {
	NodoPila top; //ULTIMO INGRESADO, ESTA ARRIBA
	NodoPila bottom; //PRIMERO INGRESADO, ESTA EN EL ULTIMO

	public Pila() {
		this.top = null;
		this.bottom = null;
	}

	public boolean isEmpty() {
		return this.bottom == null; //SI EL PRIMERO ES NULLO
	}

	public void push(String data) {
		if (isEmpty()) { //SI ESTA VACIA AGREGA PRIMERO
			bottom = new NodoPila(data);
			top = bottom;
		} else { //SI HAY ALGO, QUE EL TOP SEA LA ULTIMA INGRESADA
			NodoPila nuevo = new NodoPila(data);
			top.setNext(nuevo);
			top = nuevo;
		}
	}

	public void print() {
		if (!isEmpty()) {
			print(bottom);
		}
	}



	private void print(NodoPila next) {
		if (next.getNext() == null) {
			System.out.print(next.getData());
		} else {
			print(next.getNext());
			System.out.print(next.getData());
		}
	}

	public String peek() {
		return this.top.getData();
	}

	public void pop() {
		if (!isEmpty()) {
			if (top == bottom) {
				//System.out.println("Eliminando... " + this.bottom.getData());
				bottom = null;
			} else {
				NodoPila next = bottom;
				NodoPila temp = top;
				while (next.getNext() != null && next.getNext() != top) {
					next = next.getNext();
				}
				next.setNext(null);
				top = next;
				//System.out.println("Eliminando... " + temp.getData());
			}
		}
	}

}
