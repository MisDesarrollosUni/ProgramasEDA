package mx.edu.utez.Clase_2;

public class Hombre extends Persona {

	boolean servicio;

	// ES EL CONSTRUCTOR DE LA CLASE PADRE
	public Hombre(String nombre, int edad, String direccion,boolean servicio) {
		super(nombre, edad, direccion);
		this.servicio = servicio;
	}

	@Override
	//LOS GET Y SET YA LOS TIENE PORQUE LOS TIENE LA CLASE PADRES
	//getClass().getName() TRAEMOS EL NOMBRE DE LA CLASE
	public void mostrarDatos() {
		System.out.println("Datos "+ getClass().getSimpleName()+ ": "+getNombre()+" | "+getEdad()+" | "+getDireccion());
		System.out.println( (this.servicio?"Si hizo el SMN":"No hizo el SMN") );
	}
}
