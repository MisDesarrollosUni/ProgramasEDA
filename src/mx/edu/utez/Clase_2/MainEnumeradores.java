package mx.edu.utez.Clase_2;

public class MainEnumeradores {
	enum cafe{LATTE(true,true,26), EXPRESSO(false,true,54), CAPPUCCINO(true,true,23), AMERICANO(false,false,87);
		private boolean azucar;
		private boolean leche;
		double precio;

		cafe(boolean azucar, boolean leche, double precio) {
			this.azucar = azucar;
			this.leche = leche;
			this.precio = precio;
		}

		public boolean isAzucar() {
			return azucar;
		}

		public void setAzucar(boolean azucar) {
			this.azucar = azucar;
		}

		public boolean isLeche() {
			return leche;
		}

		public void setLeche(boolean leche) {
			this.leche = leche;
		}

		public double getPrecio() {
			return precio;
		}

		public void setPrecio(double precio) {
			this.precio = precio;
		}
	}



	public static void main(String[] args) {
		for (cafe vale: cafe.values()){
			System.out.println(vale + " cuesta $"+vale.getPrecio());
			System.out.println(vale.isAzucar()?"Tiene Azúcar":"No lleva Azúcar" );
			System.out.println(vale.isLeche()?"Tiene Leche":"No lleva Leche" );
			System.out.println("");
		}
	}
}
