package mx.edu.utez.Clase_2;

public class MainInterface {
	public static void main(String[] args) {
		Padre p1 = new Padre(true);
		Padre p2 = new Padre(false);
		Padre p3 = new Padre(true);

		p1.metodoHijo();
		p1.metodoPadre();
		System.out.println("");

		p2.metodoHijo();
		p2.metodoPadre();
		System.out.println("");

		p3.metodoHijo();
		p3.metodoPadre();
	}
}
