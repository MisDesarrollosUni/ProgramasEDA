package mx.edu.utez.Clase_2;

public class Mujer extends Persona{

	// ES EL CONSTRUCTOR DE LA CLASE PADRE
	public Mujer(String nombre, int edad, String direccion) {
		super(nombre, edad, direccion);
	}

	@Override
	public void mostrarDatos() {
		System.out.println("Datos "+ getClass().getSimpleName()+ ": "+getNombre()+" | "+getEdad()+" | "+getDireccion());
	}
}
