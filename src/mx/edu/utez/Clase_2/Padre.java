package mx.edu.utez.Clase_2;

public class Padre implements Hijo{
	//IMPLEMEN, ES PARA HACER USO DE ESA INTERFAZ, ES EL EQUIVALENTE A EXTENDS

	boolean hijos;

	public Padre(boolean hijos) {
		this.hijos = hijos;
	}

	@Override
	//ESTE MÉTODO ES DE LA INTERFAZ HIJO
	public void metodoHijo() {
		System.out.println("Todos somos hijos del alguien");
	}


	public void metodoPadre(){
		System.out.println(this.hijos?"Si eres padre":"Solo eres hijo");
	}
}
