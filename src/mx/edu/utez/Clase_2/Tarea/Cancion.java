package mx.edu.utez.Clase_2.Tarea;

public class Cancion implements Catalogo {
	private String titulo;
	private String cantante;
	private String formato;
	private double duracion;

	public Cancion(String titulo, String cantante, String formato, double duracion) {
		this.titulo = titulo;
		this.cantante = cantante;
		this.formato = formato;
		this.duracion = duracion;
	}

	@Override
	public void mostrarCatalogo() {
		System.out.println(getClass().getSimpleName()+": "+getTitulo()+" | "+getCantante()+" | "+getFormato()+" | "+getDuracion()+" minutos");
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCantante() {
		return cantante;
	}

	public void setCantante(String cantante) {
		this.cantante = cantante;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public double getDuracion() {
		return duracion;
	}

	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}
}
