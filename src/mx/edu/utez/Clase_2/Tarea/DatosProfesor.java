package mx.edu.utez.Clase_2.Tarea;

import java.util.ArrayList;
import java.util.List;

public class DatosProfesor {

	public List<ProfesorCompleto> datosProfesorC(){
		ProfesorCompleto[] profesorCompleto = new ProfesorCompleto[2];
		List<ProfesorCompleto> profesoresCompletos = new ArrayList<>();
		profesorCompleto[0] = new ProfesorCompleto("Brandon Garcia Saldaña", "Español", "Licenciado",300);
		profesorCompleto[1] = new ProfesorCompleto("Hector Saldaña Espinoza", "Programación", "Ingenieria",200);
		profesoresCompletos.add(profesorCompleto[0]);
		profesoresCompletos.add(profesorCompleto[1]);
		return profesoresCompletos;
	}

	public List<ProfesorHorario> datosProfesorH(){
		ProfesorHorario[] profesorHorario = new ProfesorHorario[2];
		List<ProfesorHorario> profesoresHorarios = new ArrayList<>();
		profesorHorario[0] = new ProfesorHorario("Evelyn Gil Bahena","Fundamentos de TI","Ingenieria",100, 10 );
		profesorHorario[1] = new ProfesorHorario("Ma. Berenice Torres Tuco","Fundamentos de Redes","Ingenieria",150,10);
		profesoresHorarios.add(profesorHorario[0]);
		profesoresHorarios.add(profesorHorario[1]);
		return  profesoresHorarios;
	}
}
