package mx.edu.utez.Clase_2.Tarea;

public class MainCanciones {

	enum formato{WAV,MP3,MIDI, AVI, MOV,MPG,DVD;}

	public static void main(String[] args) {
		Cancion[] canciones = new Cancion[5];
		canciones[0] = new Cancion("You Only Live Once","The Strokes", formato.MIDI.name(),3.11);
		canciones[1] = new Cancion("Yellow","Coldplay", formato.AVI.name(),4.32);
		canciones[2] = new Cancion("Sign of the Times","Harry Styles", formato.DVD.name(),5.42);
		canciones[3] = new Cancion("How Deep is Your Love","Bee Gees", formato.MOV.name(),4.0);
		canciones[4] = new Cancion("[They Long To Be] Close To You","Carpenters", formato.MP3.name(),3.43);

		System.out.println("\tCATÁLOGO");
		for (Cancion song:canciones){
			song.mostrarCatalogo();
		}

	}
}
