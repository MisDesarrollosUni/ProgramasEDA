package mx.edu.utez.Clase_2.Tarea;
import java.util.List;

public class MainProfesores {
	public static void main(String[] args) {
		DatosProfesor datosProfesor = new DatosProfesor();
		List<ProfesorCompleto> profesoresCompletos = datosProfesor.datosProfesorC();
		List<ProfesorHorario> profesoresHorarios = datosProfesor.datosProfesorH();

		for (ProfesorCompleto pC : profesoresCompletos) {
			pC.mostrarInformacion();
		}
		System.out.println("");
		for (ProfesorHorario pH : profesoresHorarios) {
			pH.mostrarInformacion();
		}


	}
}
