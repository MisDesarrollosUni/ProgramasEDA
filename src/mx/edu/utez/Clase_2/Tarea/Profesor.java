package mx.edu.utez.Clase_2.Tarea;

public abstract class Profesor {
	private String nombreCompleto;
	private String nombreMateria;
	private String titulo;
	private double sueldoBase;

	public Profesor(String nombreCompleto, String nombreMateria, String titulo, double sueldoBase) {
		this.nombreCompleto = nombreCompleto;
		this.nombreMateria = nombreMateria;
		this.titulo = titulo;
		this.sueldoBase = sueldoBase;
	}

	abstract public void mostrarInformacion();

	abstract public double calcularSueldo();

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getNombreMateria() {
		return nombreMateria;
	}

	public void setNombreMateria(String nombreMateria) {
		this.nombreMateria = nombreMateria;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSueldoBase() {
		return sueldoBase;
	}

	public void setSueldoBase(double sueldoBase) {
		this.sueldoBase = sueldoBase;
	}
}
