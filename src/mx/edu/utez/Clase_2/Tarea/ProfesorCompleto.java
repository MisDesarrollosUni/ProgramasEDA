package mx.edu.utez.Clase_2.Tarea;

public class ProfesorCompleto extends Profesor{

	public ProfesorCompleto(String nombreCompleto, String nombreMateria, String titulo, double sueldoBase) {
		super(nombreCompleto, nombreMateria, titulo, sueldoBase);
	}

	@Override
	public double calcularSueldo() {
		return getSueldoBase()*15;
	}

	@Override
	public void mostrarInformacion() {
		System.out.println(getClass().getSimpleName()+": "+getNombreCompleto()+" | "+getNombreMateria()+" | "+getTitulo()+" | "+getSueldoBase()+" | $"+ calcularSueldo());
	}


}
