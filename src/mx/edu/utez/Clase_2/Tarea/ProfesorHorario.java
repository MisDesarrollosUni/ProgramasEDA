package mx.edu.utez.Clase_2.Tarea;

public class ProfesorHorario extends Profesor {

	private double horasQuincenales;

	public ProfesorHorario(String nombreCompleto, String nombreMateria, String titulo, double sueldoBase, double horasQuincenales) {
		super(nombreCompleto, nombreMateria, titulo, sueldoBase);
		this.horasQuincenales = horasQuincenales;
	}

	@Override
	public double calcularSueldo() {
		return getSueldoBase()*horasQuincenales;
	}

	@Override
	public void mostrarInformacion() {
		System.out.println(getClass().getSimpleName()+": "+getNombreCompleto()+" | "+getNombreMateria()+" | "+getTitulo()+" | "+getSueldoBase()+" | $"+calcularSueldo());
	}


}
