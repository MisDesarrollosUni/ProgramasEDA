package mx.edu.utez.Clase_20;

import java.util.List;
import java.util.Stack;

public class Conversiones {

	public void imprimir(Stack<Object> miPila){
		for (Object con: miPila) {
			System.out.println(con + " -> ");
		}
	}

	public void listaPila(List<Object> miLista){
		Stack<Object> miPila = new Stack();
		for (Object objeto: miLista)
			miPila.push(objeto);
		imprimir(miPila);
	}

	public void arregloPila(Object[] miArreglo){
		Stack<Object> miPila = new Stack();
		for (Object objeto: miArreglo)
			miPila.push(objeto);
		imprimir(miPila);
	}


}
