package mx.edu.utez.Clase_20;

import mx.edu.utez.Clase_19.Pila;

import java.util.Scanner;

public class Fibonacci {
	private Pila miPila = new Pila();
	private Scanner leer = new Scanner(System.in);
	private int[] numeros;
	private int tope;

	public void finbonacci() {
		int num1 = 0, num2 = 1, num3 = 0;
		for (int i = 1; i <= tope; i++) {
			numeros[i-1]= num3;
			num3 = num1 + num2;
			num1 = num2;
			num2 = num3;
		}
		convertir();
	}

	public void convertir(){
		for (int i = numeros.length-1; i>=0;i--){
			miPila.push(""+numeros[i]);
		}
	}

	public void imprimir() {
		miPila.print();
	}

	public void validarNumero() {
		boolean flag = false;
		do {
			try {
				System.out.print("Ingrese número tope: ");
				tope = leer.nextInt();
				if(tope>0){
					numeros = new int[tope];
					flag = true;
				}

			} catch (Exception e) {
				System.out.print("Solo valores mayores a 0: ");
				leer.next();
			}
		} while (!flag);
	}

}
