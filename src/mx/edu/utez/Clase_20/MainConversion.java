package mx.edu.utez.Clase_20;

import java.util.ArrayList;
import java.util.List;

public class MainConversion {
	public static void main(String[] args) {
		Conversiones con = new Conversiones();
		System.out.println("LISTA A PILA");
		List<Object> listaEnteros = new ArrayList<>();
		listaEnteros.add(5);
		listaEnteros.add(4);
		listaEnteros.add(3);
		listaEnteros.add(2);
		listaEnteros.add(1);
		listaEnteros.add(0);
		con.listaPila(listaEnteros);

		System.out.println("\n\nARREGLO A PILA");
		Object[] arregloEnteros = {1, 5, 8, 7, 4, 3, 6, 9, 7};
		con.arregloPila(arregloEnteros);
	}
}
