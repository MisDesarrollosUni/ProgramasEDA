package mx.edu.utez.Clase_21;

import mx.edu.utez.Clase_19.Pila;

import java.util.ArrayList;
import java.util.List;

public class ListasRandomPilas {
	private List<Integer> listaUno = new ArrayList<>();
	private List<Integer> listaDos = new ArrayList<>();
	private List<Integer> listaTres = new ArrayList<>();
	private Pila pila = new Pila();

	public void imprimir() {
		System.out.println("Lista 1:");
		for (int i = 0; i < 5; i++)
			System.out.print(listaUno.get(i) + " -> ");
		System.out.println("\n\nLista 2:");
		for (int i = 0; i < 5; i++)
			System.out.print(listaDos.get(i) + " -> ");
		System.out.println("\n\nLista fusionada (Pila):");
		pila.print();
	}

	public void numerosAleatorios() {
		int i = 0;
		for (; i < 5; i++) {
			listaUno.add((int) Math.ceil(Math.random() * 20));
			listaDos.add((int) Math.ceil(Math.random() * 20));
		}
		llenarElementosPila(--i);
	}

	public void llenarElementosPila(int tam) {
		if (tam < 0) {
			return;
		} else {
			llenarElementosPila(tam - 1);
			pila.push("" + (listaUno.get(tam) * listaUno.get(tam)));
			pila.push("" + (listaDos.get(tam) * listaDos.get(tam)));
		}
	}

}
