package mx.edu.utez.Clase_21;

public class Mascota {
	private String nombre;
	private int edad;
	private String nombreDueño;
	private String tipo;

	public Mascota() {
	}

	public Mascota(String nombre, int edad, String nombreDueño, String tipo) {
		this.nombre = nombre;
		this.edad = edad;
		this.nombreDueño = nombreDueño;
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombreDueño() {
		return nombreDueño;
	}

	public void setNombreDueño(String nombreDueño) {
		this.nombreDueño = nombreDueño;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
