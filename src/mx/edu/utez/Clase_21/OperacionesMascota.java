package mx.edu.utez.Clase_21;

import java.util.Scanner;
import java.util.Stack;

public class OperacionesMascota {
	Scanner leer = new Scanner(System.in);
	Stack<Mascota> mascotas = new Stack<>();
	Mascota nuevaMascota = new Mascota();

	public void inicio() {
		int opc;
		System.out.println("\n" +
				"░█▀▄▀█ ─█▀▀█ ░█▀▀▀█ ░█▀▀█ ░█▀▀▀█ ▀▀█▀▀ ─█▀▀█ ░█▀▀▀█ \n" +
				"░█░█░█ ░█▄▄█ ─▀▀▀▄▄ ░█─── ░█──░█ ─░█── ░█▄▄█ ─▀▀▀▄▄ \n" +
				"░█──░█ ░█─░█ ░█▄▄▄█ ░█▄▄█ ░█▄▄▄█ ─░█── ░█─░█ ░█▄▄▄█");
		do {
			System.out.println("\n¿DESEAS INGRESAR UNA MASCOTA + ?");
			System.out.println("\t1. SI\n\t2. NO");
			System.out.println("Elija: ");
			opc = validarNumero();
			switch (opc) {
				case 1:
					addMascota();
					break;
				case 2:
					printMascotas();
					opc = 2;
					break;
				default:
					System.out.println("\tESA OPCIÓN NO EXISTE");
			}
		} while (opc != 2);
	}

	public void printMascotas() {
		int i = 1;
		for (Mascota pet : mascotas) {
			System.out.println("\n\tMASCOTA " + (i++));
			System.out.println("NOMBRE: " + pet.getNombre());
			System.out.println("EDAD: " + pet.getEdad() + " MESES");
			System.out.println("DUSEÑO: " + pet.getNombreDueño());
			System.out.println("TIPO: " + pet.getTipo());
		}
	}

	public void addMascota() {
		System.out.print("INGRESE EL NOMBRE DE LA MASCOTA: ");
		nuevaMascota.setNombre(leer.next());
		System.out.println("INGRESE LA EDAD (MESES): ");
		nuevaMascota.setEdad(validarNumero());
		System.out.print("INGRESE EL NOMBRE DEL DUEÑO: ");
		nuevaMascota.setNombreDueño(leer.next());
		System.out.print("INGRESE EL TIPO DE MASCOTA: ");
		nuevaMascota.setTipo(leer.next());
		mascotas.push(nuevaMascota);
	}


	public int validarNumero() {
		boolean flag = false;
		int opc = 0;
		do {
			try {
				System.out.print(": ");
				opc = leer.nextInt();
				if (opc > 0) flag = true;
			} catch (Exception e) {
				System.out.println("Solo números válidos");
				leer.next();
			}
		} while (!flag);
		return opc;
	}
}
