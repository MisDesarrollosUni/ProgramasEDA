package mx.edu.utez.Clase_22;

import mx.edu.utez.Clase_19.Pila;

import java.util.Scanner;

public class InfijaPostfija {
	private Pila miPila = new Pila();
	private Scanner leer = new Scanner(System.in);
	private String expresionUsuario;
	private Pila expresionResultado = new Pila();
	private String OPE = "/*-+";

	public void inicio() {
		System.out.println("\tINFIJA A POSTFIJA O SUFIJA");
		System.out.print("Ingresa la expresión: ");
		expresionUsuario = leer.next();
		convertirExpresion();
	}

	public void voltearCadena(String expresionTemp){
		for (int i = expresionTemp.length()-1;i>=0;i--)
			expresionResultado.push(""+expresionTemp.charAt(i));
		expresionResultado.print();
	}

	public void convertirExpresion(){
		String expresionTemporal = "";
		for (int i = 0; i< expresionUsuario.length(); i++) {
			char carac = expresionUsuario.charAt(i);
			if (carac == '(' || OPE.indexOf(carac) != -1) {
				miPila.push("" + carac);
			} else if (carac == ')') {
				while (!miPila.peek().equals("(")) {
					expresionTemporal += miPila.peek();
					miPila.pop();
				}
				miPila.pop();
			} else {
				expresionTemporal += carac;
			}
		}
		while (!miPila.isEmpty()){
			expresionTemporal += miPila.peek();
			miPila.pop();
		}
		voltearCadena(expresionTemporal);
	}
}
