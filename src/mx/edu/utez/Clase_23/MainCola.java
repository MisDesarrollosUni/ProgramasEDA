package mx.edu.utez.Clase_23;

import java.util.*;

public class MainCola {
    public static void main(String[] args) {
       /* Queue<String> cola = new LinkedList<>();
        cola.add("String 1");
        cola.add("String 2");
        cola.add("String 3");
        cola.add("String 4");
        System.out.println("Tamaño es: "+cola.size()); // NOS TRAE EL TAMAÑO
        System.out.println("Extraer primer elemento es: "+cola.poll()); //LO QUITA DE LA COLA Y TE DICE QUIEN FUE
        System.out.println("Tamaño es: "+cola.size());
        System.out.println("Consultar primer elemento: "+cola.peek()); // SOLAMENTE LO CONSULTA, NO ELIMINA
        System.out.println("Tamaño es: "+cola.size());
        System.out.println("Elimino: "+cola.remove()); //LO ELIMINAR PERO NO TE DICE QUIEN FUE REMOVIDO
        //cola.clear(); // ELIMINA TODA LA COLA
        System.out.println("Tamaño es: "+cola.size());

        // COLA SIN PRIORIDAD
        Queue<Integer> cola2 = new LinkedList<>();
        cola2.add(14);
        cola2.add(15);
        cola2.add(1);
        cola2.add(8);
        cola2.add(9);
        System.out.println("IMPRESION SIN COLA DE PRIORIDAD");
        for (int n: cola2) {
            System.out.println(n);
        }
        System.out.println("--------------------------------");

        // COLA CON PRIORIDAD
        PriorityQueue<Integer> cola3 = new PriorityQueue();
        cola3.add(14);
        cola3.add(15);
        cola3.add(1);
        cola3.add(8);
        cola3.add(9);
        System.out.println("IMPRESION SIN COLA DE PRIORIDAD AUN TENIENDO PRIORIDAD");
        for (int n: cola3) {
            System.out.println(n);
        }
        System.out.println("--------------------------------");
        System.out.println("IMPRESION CON COLA DE PRIORIDAD DEPENDIENDO EN CODIGO ASCII o NUMERO");
        while(!cola3.isEmpty()){
            System.out.println(cola3.poll());
        }
*/

        PriorityQueue<String> cola4 = new PriorityQueue<>();
        cola4.add("Silvia");
        cola4.add("Manuel");
        cola4.add("Esmeralda");
        cola4.add("Hugo");
        cola4.add("Hector");
        cola4.add("Sebastian");
        for (String n:cola4)
            System.out.println(n);
        System.out.println("--------------------------------");
        while (!cola4.isEmpty())
            System.out.println(cola4.poll()); // SE TIENE QUE USAR POLL PARA VER QUIEN SALE PRIMERO DE LA PRIORIDAD
    }

}
