package mx.edu.utez.Clase_24;

public class MainArbol {
    public static void main(String[] args) {
        Tree arbol = new Tree();
        arbol.add(60);
        arbol.add(41);
        arbol.add(16);
        arbol.add(25);
        arbol.add(53);
        arbol.add(46);
        arbol.add(42);
        arbol.add(55);
        arbol.add(74);
        arbol.add(65);
        arbol.add(63);
        arbol.add(62);
        arbol.add(64);
        arbol.add(70);

        System.out.print("PREORDEN: ");
        arbol.printPreorden();

        System.out.print("\nINORDEN: ");
        arbol.printInorden();

        System.out.print("\nPOSTORDEN: ");
        arbol.printPostorden();
    }
}
