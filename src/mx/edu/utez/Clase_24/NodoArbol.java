package mx.edu.utez.Clase_24;

public class NodoArbol {
    int value;
    NodoArbol left;
    NodoArbol rigth;

    public NodoArbol(int value) {
        this.value = value;
        this.left = null;
        this.rigth = null;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public NodoArbol getLeft() {
        return left;
    }

    public void setLeft(NodoArbol left) {
        this.left = left;
    }

    public NodoArbol getRigth() {
        return rigth;
    }

    public void setRigth(NodoArbol rigth) {
        this.rigth = rigth;
    }
}
