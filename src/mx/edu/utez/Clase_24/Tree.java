package mx.edu.utez.Clase_24;

public class Tree {
    NodoArbol root;

    public Tree() {
        this.root = null;
    }

    public boolean isEmpty() {
        return this.root == null;
    }

    public void add(int value) {
        NodoArbol nuevo = new NodoArbol(value);
        if (isEmpty()) {
            this.root = nuevo;
        } else {
            add(nuevo, this.root);
        }
    }

    private void add(NodoArbol nuevo, NodoArbol root) {
        if (nuevo.getValue() <= root.getValue()) {
            //VA A LA IZQUIERDA
            if (root.getLeft() == null) {
                root.setLeft(nuevo);
            } else {
                add(nuevo, root.getLeft());
            }
        } else {
            //VA A LA DERECHA
            if (root.getRigth() == null) {
                root.setRigth(nuevo);
            } else {
                add(nuevo, root.getRigth());
            }
        }
    }

    public void printPreorden() {
        printPreorden(this.root);
    }

    private void printPreorden(NodoArbol root) {
        if (root != null) {
            System.out.print(root.getValue() + " ");
            printPreorden(root.getLeft());
            printPreorden(root.getRigth());
        }
    }

    public void printInorden() {
        printInorden(this.root);
    }

    private void printInorden(NodoArbol root) {
        if (root != null) {
            printInorden(root.getLeft());
            System.out.print(root.getValue() + " ");
            printInorden(root.getRigth());
        }
    }

    public void printPostorden() {
        printPostorden(this.root);
    }

    private void printPostorden(NodoArbol root) {
        if (root != null) {
            printPostorden(root.getLeft());
            printPostorden(root.getRigth());
            System.out.print(root.getValue() + " ");
        }
    }


}
