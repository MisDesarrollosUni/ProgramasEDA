package mx.edu.utez.Clase_3;

public class Factorial {

	//RECURSIVIDAD DIRECTA
	public int calcularFactorial(int n){
		if(n == 0){ //CONDICIÓN DE SALIDA
			return 1;
		}else{
			//n TOMA VALORES DIFERENTES
			return n=n*calcularFactorial(n-1); //APROXIMACION AL FINAL Y LLAMADA
			//RECURSIVA
		}
	}
}
