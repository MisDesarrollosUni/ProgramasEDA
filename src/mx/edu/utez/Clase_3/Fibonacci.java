package mx.edu.utez.Clase_3;

public class Fibonacci {
	public int calcularFibonacci(int num){
		if(num == 0 || num == 1) {
			return num;
		}else{
			return calcularFibonacci(num-1)+calcularFibonacci(num-2);
		}
	}
}
