package mx.edu.utez.Clase_3;

import java.util.Scanner;

public class MainPotencia {
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		Potencia pt = new Potencia();
		long base, expo;
		System.out.println("ELEVAR NÚMERO A POTENCIA");
		System.out.print("Ingrese número base: ");
		base = leer.nextLong();
		System.out.print("Ingrese potencia: ");
		expo = leer.nextLong();
		System.out.println("Resultado "+base+"^"+expo+" es: " + pt.calcularPotencia(base,expo));
	}
}
