package mx.edu.utez.Clase_3;

import java.util.Scanner;

public class MainSumatoria {
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		Sumatoria sm = new Sumatoria();
		double numero = 0;
		System.out.println("SUMA NÚMEROS NATURALES HASTA N");
		System.out.print("Ingrese el número entero: ");
		numero = leer.nextDouble();
		if(numero<=10500&&numero>0){
			System.out.println("Suma de: " + sm.sumaNumeros(numero));
		}else{
			System.out.println("No se puede por la cantidad máxima de recursiones");
		}
	}
}
