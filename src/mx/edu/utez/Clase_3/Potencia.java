package mx.edu.utez.Clase_3;

public class Potencia {
	public long calcularPotencia(long base, long expo){
		if(expo == 0){
			return 1;
		}else{
			return base * calcularPotencia(base,expo-1);
		}
	}
}
