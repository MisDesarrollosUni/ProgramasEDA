package mx.edu.utez.Clase_4;

import java.util.Scanner;

public class MainPromedios {
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		Promedio pm = new Promedio();
		String numMaterias;
		int num = 0;
		System.out.println("\tCALCULAR PROMEDIO");
		System.out.print("# de Materias: ");
		numMaterias = leer.next();
		if (numMaterias.matches("[0-9]*")) {
			num = Integer.parseInt(numMaterias);
			if (num > 0)
				System.out.println("Promedio: " + pm.calcularPromedio(num, 0, 0));
			else
				System.out.println("Número de materias no válido");
		} else
			System.out.println("Número de materias no válido");
	}
}
