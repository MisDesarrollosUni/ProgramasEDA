package mx.edu.utez.Clase_4;

public class ParImpar {
	public boolean par(int num) {
		if (num == 0) return true;
		 else return impar(num - 1);
	}

	public boolean impar(int num) {
		if (num == 0) return false;
		 else return par(num - 1);
	}
}
