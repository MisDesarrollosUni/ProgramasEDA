package mx.edu.utez.Clase_4;

public class Primo {

	public boolean calcularPrimo(int num, int div) {
		if (num / 2 < div)
			return true;
		else if(num%div==0)
			return false;
		else
			return calcularPrimo(num,div+1);
	}

}
