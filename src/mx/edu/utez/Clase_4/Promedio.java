package mx.edu.utez.Clase_4;

import java.util.Scanner;

public class Promedio {

	public double calcularPromedio(int materias, double suma, int i) {
		Scanner leer = new Scanner(System.in);
		if (materias == 0)
			return suma / i;
		else {
			++i;
			System.out.print("Calificación " + i + ": ");
			return calcularPromedio(materias - 1, suma + leer.nextDouble(), i++);
		}
	}


}
