package mx.edu.utez.Clase_4;

import java.util.Scanner;

public class mainParImpar {
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		ParImpar pi = new ParImpar();
		String numero;
		int num;
		System.out.println("\tNÚMERO PAR E IMPAR");
		System.out.print("Ingrese Número: ");
		numero = leer.next();
		if (numero.matches("[0-9]*")){
			num = Integer.parseInt(numero);
			System.out.println(pi.par(num) ? "Es par" : "Es impar");
		}else
			System.out.println("Número no válido");
	}
}
