package mx.edu.utez.Clase_5;

public class Cuadrado {
	public int calcularCuadrado(int num, int expo) {
		if (expo == 0) {
			return 1;
		} else {
			return num*calcularCuadrado(num,expo-1);
		}
	}
}
