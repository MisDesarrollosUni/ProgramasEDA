package mx.edu.utez.Clase_5;

public class EstudianteBean {
	String nombre;

	public EstudianteBean() {
	}

	public EstudianteBean(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
