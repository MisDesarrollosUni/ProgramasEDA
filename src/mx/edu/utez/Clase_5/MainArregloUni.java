package mx.edu.utez.Clase_5;

import java.util.Scanner;

public class MainArregloUni {
	public static void main(String[] args) {
		String[] nombres = new String[5];
		//nombres[0] = "Raul";
		//nombres[1] = "Hector";

		Scanner leer = new Scanner(System.in);
		for (int i = 0; i < nombres.length; i++) {
			System.out.print("¿Cúal es tu nombre: ");
			nombres[i] = leer.nextLine();
		}

		for (int i = 0; i < nombres.length; i++) {
			System.out.println("Nombre "+(i+1)+": "+nombres[i]);

		}

		for(String nombre: nombres){
			System.out.println(nombre);
		}
	}
}
