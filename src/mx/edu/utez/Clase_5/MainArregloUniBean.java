package mx.edu.utez.Clase_5;


import java.util.Scanner;

public class MainArregloUniBean {
	public static void main(String[] args) {
		EstudianteBean[] estudiantes = new EstudianteBean[5];
		estudiantes[0] = new EstudianteBean("Hector");
		EstudianteBean e2 = new EstudianteBean("Silvia");
		estudiantes[1] = e2;
		EstudianteBean e3 = new EstudianteBean();
		e3.setNombre("Andrea");
		estudiantes[2] = e3;
		Scanner leer = new Scanner(System.in);
		for (int i = 3; i < estudiantes.length; i++){
			System.out.print("¿Cuál es el nombre: ");
			EstudianteBean e = new EstudianteBean();
			e.setNombre(leer.nextLine());
			estudiantes[i] = e;
		}
		for(EstudianteBean eb : estudiantes){
			System.out.println("Nombre: "+eb.getNombre());
		}


	}
}
