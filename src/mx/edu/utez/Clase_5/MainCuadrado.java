package mx.edu.utez.Clase_5;

public class MainCuadrado {
	public static void main(String[] args) {
		int[] numeros = new int[10];
		Cuadrado cuadrado = new Cuadrado();
		for (int i = 1;i<=numeros.length;i++){
			numeros[i-1]  = cuadrado.calcularCuadrado(i,2);
		}
		for(int num : numeros){
			System.out.print("["+num+"] ");
		}
	}
}
