package mx.edu.utez.Clase_6;

import java.util.Scanner;

public class LetrasPalabras {
	public void metodoPalabras(){
		Scanner leer = (new Scanner(System.in));
		String palabra, input;
		System.out.print("Indica la palabra: ");
		palabra = leer.next();
		palabra = palabra.toUpperCase();
		char[] caracteres = palabra.toCharArray();
		String[] palabras = new String[caracteres.length];
		for(int i = 0; i<caracteres.length;i++){
			boolean flag = false;
			String letraC = ""+caracteres[i];
			do{
				System.out.print("Palabra con "+caracteres[i]+": ");
				input = leer.next();
				String letraI = ""+input.charAt(0);
				if(letraC.equalsIgnoreCase(letraI)){
					palabras[i] = input;
					flag=true;
				}
			}while(!flag);
		}
		System.out.println("\nLa palabra fue: " + palabra);
		System.out.println("Y las palabras fueron: ");
		for(String p: palabras){
			System.out.println(p);
		}
	}
}
