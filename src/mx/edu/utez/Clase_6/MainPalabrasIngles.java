package mx.edu.utez.Clase_6;

import java.util.Scanner;

public class MainPalabrasIngles {
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		PalabrasIngles palabrasIngles = new PalabrasIngles();
		String palabra, traduccion;
		System.out.print("Palabra a traducir: ");
		palabra = leer.next();
		traduccion = palabrasIngles.traducir(palabra);
		System.out.println(traduccion);
	}
}
