package mx.edu.utez.Clase_6;

public class PalabrasIngles {

	String[][] diccionario = new String[20][2];

	public void cargar(){
		diccionario[0][0] = "Lapiz";diccionario[0][1] = "Pencil";
		diccionario[1][0] = "Pluma";diccionario[1][1] = "Pen";
		diccionario[2][0] = "Regla";diccionario[2][1] = "Ruler";
		diccionario[3][0] = "Goma";diccionario[3][1] = "Eraser";
		diccionario[4][0] = "Cuaderno";diccionario[4][1] = "Noteboook";
		diccionario[5][0] = "Casa";diccionario[5][1] = "House";
		diccionario[6][0] = "Agua";diccionario[6][1] = "Water";
		diccionario[7][0] = "Muro";diccionario[7][1] = "Wall";
		diccionario[8][0] = "Guitarra";diccionario[8][1] = "Guitar";
		diccionario[9][0] = "Espejo";diccionario[9][1] = "Mirror";
		diccionario[9][0] = "Espejo";diccionario[9][1] = "Mirror";
		diccionario[11][0] = "Silla";diccionario[11][1] = "Chair";
		diccionario[12][0] = "Arbol";diccionario[12][1] = "Tree";
		diccionario[13][0] = "Uno";diccionario[13][1] = "One";
		diccionario[14][0] = "Dos";diccionario[14][1] = "Two";
		diccionario[15][0] = "Tres";diccionario[15][1] = "Three";
		diccionario[16][0] = "Cuatro";diccionario[16][1] = "Four";
		diccionario[17][0] = "Cinco";diccionario[17][1] = "Five";
		diccionario[18][0] = "Seis";diccionario[18][1] = "Six";
		diccionario[19][0] = "Diez";diccionario[19][1] = "Ten";

	}

	public String traducir(String palabra){
		cargar();
		String traduccion="";
		for( int i = 0; i<diccionario.length;i++){
			if(palabra.equalsIgnoreCase(diccionario[i][0])){
				traduccion = diccionario[i][1];
				break;
			}else{
				traduccion= "No tiene traducción";
			}
		}

		return traduccion;
	}

}
