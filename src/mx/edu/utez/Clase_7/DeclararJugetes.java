package mx.edu.utez.Clase_7;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DeclararJugetes {

	Juguete[] juguetes = new Juguete[5];

	public void declararDatos() {
		Scanner leer = new Scanner(System.in);
		leer.useDelimiter("\n");
		String nombre, marca;
		double costo;
		for (int i = 0; i < juguetes.length; i++) {
			System.out.println("\nJUGUETE " + (i + 1));
			System.out.print("Nombre: ");
			nombre = leer.nextLine();
			System.out.print("Marca: ");
			marca = leer.nextLine();
			costo = validarCosto();
			juguetes[i] = new Juguete(nombre, marca, costo);
		}
		imprimir();
	}

	public double validarCosto() {
		Scanner leer = new Scanner(System.in);
		double costo = 0;
		boolean flag = false;
		do {
			try {
				System.out.print("Costo: $");
				costo = leer.nextDouble();
				if (costo > 0) {
					flag = true;
				}
			}catch (InputMismatchException ex){
				leer.next();
			}
		} while (!flag);
		return costo;
	}

	public void imprimir() {
		System.out.println("");
		for (Juguete j : juguetes) {
			System.out.println(j.getNombre() + " | " + j.getMarca() + " | " + j.getCosto());
		}
	}

}
