package mx.edu.utez.Clase_7;

public class EjemploNumeros {

	public int[][] llenarArreglo() {
		int[][] numeros = new int[4][4];
		int contador = 1;
		for (int i = 0; i < numeros.length; i++) {
			for (int j = 0; j < numeros[i].length; j++) {
				numeros[i][j] = contador;
				contador++;
			}
		}
		return numeros;
	}

	public void imprimirArreglo(int [][] numeros){
		for (int i = 0; i < numeros.length; i++) {
			for (int j = 0; j < numeros[i].length; j++) {
				System.out.print(numeros[i][j]+" ");
			}
			System.out.println();
		}
	}
}
