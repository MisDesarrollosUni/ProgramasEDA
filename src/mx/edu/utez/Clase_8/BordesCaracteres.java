package mx.edu.utez.Clase_8;

public class BordesCaracteres {
	public int tamaño = 5;
	public char[][] matriz = new char[tamaño][tamaño];

	public void llenarMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (char) (Math.random() * (90 - 65 + 1) + 65);
			}
		}
	}

	public void realizarBordes() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (j == 0 || j == matriz[i].length - 1 || i == 0 || i == matriz.length - 1) {
					matriz[i][j] = '-';
				}
			}
		}
	}

	public void imprimirMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + "  ");
			}
			System.out.println();
		}
	}

}
