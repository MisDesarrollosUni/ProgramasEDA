package mx.edu.utez.Clase_8;

public class Caracteres {
	public int tamaño = 5;
	public char[][] matriz = new char[tamaño][tamaño];

	public void llenarMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (char) (Math.random() * (90 - 65 + 1) + 65);
			}
		}
	}

	public void llenarDiagonal() {
		for (int i = 0; i < matriz.length; i++) {
			matriz[i][i] = '-';
		}
	}

	public void imprimirMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + "  ");
			}
			System.out.println();
		}
	}
	

}
