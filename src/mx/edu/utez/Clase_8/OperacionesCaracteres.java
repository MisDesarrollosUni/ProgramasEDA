package mx.edu.utez.Clase_8;

public class OperacionesCaracteres {
	private int tamaño = 6;
	private char[][] matriz = new char[tamaño][tamaño];
	private int[] contadorVocales = new int[tamaño];
	private String buscarVocal = "AEIOUaeiou";


	public void llenarMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (char) (Math.random() * (90 - 65 + 1) + 65);
			}
		}
	}

	public void imprimirMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + "  ");
			}
			System.out.println();
		}
	}

	public void reemplazarVocales() {
		int contador = 0;
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (buscarVocal.indexOf(matriz[i][j]) != -1) {
					matriz[i][j] = '-';
					contador++;
				}
			}
			contadorVocales[i] = contador;
			contador = 0;
		}
	}

	public void imprimirContadorVocales(){
		for (int i = 0; i < tamaño; i++) {
			System.out.println("# de Vocales en fila "+(i+1)+": "+contadorVocales[i]);
		}
	}

	public void realizarBordes() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (j == 0 || j == matriz[i].length - 1 || i == 0 || i == matriz.length - 1) {
					matriz[i][j] = '-';
				}
			}
		}
	}

	public void llenarDiagonal() {
		for (int i = 0; i < matriz.length; i++) {
			matriz[i][i] = '-';
		}
	}

}
