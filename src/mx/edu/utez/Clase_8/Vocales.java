package mx.edu.utez.Clase_8;

public class Vocales {
	public int tamaño = 8;
	public char[][] matriz = new char[tamaño][tamaño];
	public String buscar ="AEIOUaeiou";

	public void llenarMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (char) (Math.random() * (90 - 65 + 1) + 65);
			}
		}
	}

	public void imprimirMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + "  ");
			}
			System.out.println();
		}
	}

	public void reemplazarVocales(){
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if(buscar.indexOf(matriz[i][j])!=-1){
					matriz[i][j] = '-';
				}
			}

		}
	}
}
