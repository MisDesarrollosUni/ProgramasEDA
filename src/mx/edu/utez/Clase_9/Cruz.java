package mx.edu.utez.Clase_9;

import java.util.Scanner;

public class Cruz {
	private Scanner leer = new Scanner(System.in);
	private String[][] matriz;
	private int fila, columna;

	public void llenarEspacios() {
		for (String[] matriz1 : matriz) {
			for (int j = 0; j < matriz1.length; j++) {
				matriz1[j] = "  ";
			}
		}
	}

	public void pedirNumeros() {
		System.out.print("   # de Filas (MÍN 3): ");
		fila = validarNumero();
		System.out.println();
		System.out.print("# de Columnas (MÍN 3): ");
		columna = validarNumero();
		matriz = new String[fila][columna];
		llenarEspacios();
		System.out.println();
		imprimirCruz();
	}

	public int validarNumero() {
		int num = 0;
		boolean flag = false;
		do {
			try {
				num = leer.nextInt();
				if (num > 2) {
					flag = true;
				} else {
					System.out.print("   # (MÍN 3): ");
				}
			} catch (Exception e) {
				System.out.println("\tSOLO NÚMEROS");
				System.out.print("   # (MÍN 3): ");
				leer.next();
			}
		} while (!flag);
		return num;
	}

	public void imprimirCruz() {
		imprimirFila();
		imprimirColumna();

		for (String[] matriz1 : matriz) {
			for (String matriz11 : matriz1) {
				System.out.print(matriz11);
			}
			System.out.println();
		}
	}

	public void imprimirFila() {
		int f1, f2;
		if (fila % 2 == 0) {
			f1 = fila / 2;
			f2 = f1 - 1;
			for (int i = 0; i < columna; i++) {
				matriz[f1][i] = "* ";
				matriz[f2][i] = "* ";
			}
		} else {
			f1 = (fila - 1) / 2;
			for (int i = 0; i < columna; i++) {
				matriz[f1][i] = "* ";
			}
		}

	}

	public void imprimirColumna() {
		int c1, c2;
		if (columna % 2 == 0) {
			c1 = columna / 2;
			c2 = c1 - 1;
			for (int i = 0; i < fila; i++) {
				matriz[i][c1] = "* ";
				matriz[i][c2] = "* ";
			}
		} else {
			c1 = (columna - 1) / 2;
			for (int i = 0; i < fila; i++) {
				matriz[i][c1] = "* ";
			}
		}
	}
}
