package mx.edu.utez.Clase_9;

public class Empleado {

	private String nombreCompleto;
	private String nombreDepartamento;
	private double sueldo;

	public Empleado(String nombreCompleto, String nombreDepartamento, double sueldo) {
		this.nombreCompleto = nombreCompleto;
		this.nombreDepartamento = nombreDepartamento;
		this.sueldo = sueldo;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}
}
