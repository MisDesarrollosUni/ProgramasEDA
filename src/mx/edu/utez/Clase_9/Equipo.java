package mx.edu.utez.Clase_9;

import java.util.Scanner;

public class Equipo {

	private Scanner leer = new Scanner(System.in);
	private String[][] datosEquipo = new String[5][2];

	public void principal() {
		llenarDatos();
		System.out.println();
		imprimirDatos();
		equipos();
	}

	public void llenarDatos() {
		for (int i = 0; i < datosEquipo.length; i++) {
			System.out.println("\tEQUIPO " + (i + 1));
			datosEquipo[i][0] = setEquipo();
			datosEquipo[i][1] = setPuntaje();
		}
	}

	public String setEquipo() {
		leer.useDelimiter("\n");
		String nombre;
		boolean flag = false;
		do {
			System.out.print("Nombre: ");
			nombre = leer.next();
			try {
				Integer.parseInt(nombre.substring(0, 1));
				System.out.println("\tDebe iniciar con letras");
			} catch (Exception e) {
				flag = true;
			}
		} while (!flag);
		return nombre;
	}

	public String setPuntaje() {
		String puntaje;
		double puntajeEquipo = 0;
		boolean flag = false;
		do {
			try {
				System.out.print("Puntaje: ");
				puntaje = leer.next();
				puntajeEquipo = Double.parseDouble(puntaje);
				flag = true;
			} catch (Exception e) {
				System.out.println("\tDeben ser solo números");
			}
		} while (!flag);
		return "" + puntajeEquipo;
	}

	public void imprimirDatos() {
		for (int i = 0; i < datosEquipo.length; i++) {
			System.out.println("\tDATOS EQUIPO " + (i + 1));
			System.out.println("Equipo: " + datosEquipo[i][0]);
			System.out.println("Puntaje: " + datosEquipo[i][1]);
		}
	}

	public void equipos() {
		double min = 0, max = 0, valor;
		int x = 0, y = 0;

		for (int i = 0; i < datosEquipo.length; i++) {
			valor = Double.parseDouble(datosEquipo[i][1]);
			if (valor > max) {
				max = valor;
				x = i;
			}
		}
		min = max;
		for (int i = 0; i < datosEquipo.length; i++) {
			valor = Double.parseDouble(datosEquipo[i][1]);
			if (valor < min) {
				min = valor;
				y = i;
			}
		}
		System.out.println("");
		System.out.println("\tEQUIPO ALTO");
		System.out.println("Nombre: " + datosEquipo[x][0] + " |  Puntaje: " + datosEquipo[x][1]);
		System.out.println();
		System.out.println("\tEQUIPO BAJO");
		System.out.println("Nombre: " + datosEquipo[y][0] + " |  Puntaje: " + datosEquipo[y][1]);

	}

}
