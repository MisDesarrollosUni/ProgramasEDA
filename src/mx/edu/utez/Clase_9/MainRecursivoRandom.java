package mx.edu.utez.Clase_9;

public class MainRecursivoRandom {

    public static void main(String[] args) {
        RecursivoRandom rr = new RecursivoRandom();
        int tamaño = 4;
        rr.llenarMatriz(tamaño);
        System.out.println("\tIMPRESIÓN CON FOR");    
        rr.imprimirMatriz();
        
        System.out.println("\tIMPRESIÓN CON RECURSIVIDAD");
        rr.recursivo(tamaño,0);

       
    }
}
