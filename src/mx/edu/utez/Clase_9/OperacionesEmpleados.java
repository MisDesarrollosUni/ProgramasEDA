package mx.edu.utez.Clase_9;

import java.util.Scanner;

public class OperacionesEmpleados {

	private final Scanner leer = new Scanner(System.in);
	private Empleado[][] empleados = new Empleado[3][2];
	private String nombre, departamento;
	private double sueldo;

	public void datosEmpleados() {
		for (int i = 0; i < empleados.length; i++) {
			System.out.println("\tEMPLEADO " + (i + 1));
			nombre = validarNombre();
			departamento = validarDepartamento();
			sueldo = validarSueldo();
			empleados[i][0] = new Empleado(nombre, departamento, sueldo);
			System.out.println("\tJEFE DIRECTO " + (i + 1));
			nombre = validarNombre();
			departamento = validarDepartamento();
			sueldo = validarSueldo();
			empleados[i][1] = new Empleado(nombre, departamento, sueldo);
			System.out.println("______________________________");
		}

	}

	public String validarNombre() {
		String numeros = "0123456789", nombreE = "";
		leer.useDelimiter("\n");
		boolean flag = false;
		do {
			System.out.print("NOMBRE COMPLETO: ");
			nombreE = leer.next();
			if (!nombreE.isEmpty()) {

					flag = true;

			} else {
				System.out.println("\tNO DEBE ESTAR VACÍO");
			}
		} while (!flag);
		return nombreE;
	}

	public String validarDepartamento() {
		leer.useDelimiter("\n");
		String dep = "";
		boolean flag = false;
		do {
			System.out.print("NOMBRE DEPARTAMENTO: ");
			dep = leer.next();
			if (dep.isEmpty()) {
				System.out.println("\tNO DEBE ESTAR VACÍO");
			} else {
				flag = true;
			}
		} while (!flag);
		return dep;
	}

	public double validarSueldo() {
		double sueldoE = 0;
		boolean flag = false;
		do {
			try {
				System.out.print("SUELDO BASE: $");
				sueldoE = leer.nextDouble();
				flag = true;
			} catch (Exception e) {
				System.out.println("\tSOLOS NÚMEROS");
				leer.next();
			}

		} while (!flag);
		return sueldoE;
	}

	public void imprimirEmpleados() {
		System.out.println("___________IMPRESIÓN__________");
		System.out.println("______________________________");
		for (int i = 0; i < empleados.length; i++) {
			System.out.println("\tEMPLEADO " + (i + 1));
			System.out.println("NOMBRE COMPLETO: " + empleados[i][0].getNombreCompleto());
			System.out.println("NOMBRE DEPARTAMENTO: " + empleados[i][0].getNombreDepartamento());
			System.out.println("SUELDO BASE: $" + empleados[i][0].getSueldo());
			System.out.println("\tJEFE DIRECTO " + (i + 1));
			System.out.println("NOMBRE COMPLETO: " + empleados[i][1].getNombreCompleto());
			System.out.println("NOMBRE DEPARTAMENTO: " + empleados[i][1].getNombreDepartamento());
			System.out.println("SUELDO BASE: $" + empleados[i][1].getSueldo());
			System.out.println("______________________________");
		}
	}
}
