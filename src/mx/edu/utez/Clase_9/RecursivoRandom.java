package mx.edu.utez.Clase_9;

public class RecursivoRandom {

	private int[][] matriz;

	public void llenarMatriz(int tam) {
		matriz = new int[tam][tam];
		for (int i = 0; i < tam; i++) {
			for (int j = 0; j < tam; j++) {
				matriz[i][j] = (int) Math.ceil(Math.random() * 90 + 9);
			}
		}
	}

	public void imprimirMatriz() {
		for (int[] matriz1 : matriz) {
			for (int j = 0; j < matriz1.length; j++) {
				System.out.print(matriz1[j] + "  ");
			}
			System.out.println("");
		}
	}

	public void recursivo(int tamaño,  int inicio) {
		for (int i = 0; i < tamaño; i++) {
			metodoUno(i, inicio, tamaño);
			System.out.println();
		}
	}

	public int metodoUno(int i, int j, int tam) {
		if (j == tam) {
			return j;
		} else {
			System.out.print(matriz[i][j] + "  ");
			return metodoDos(i, j + 1, tam);
		}
	}

	public int metodoDos(int i, int j, int tam) {
		if (j == tam) {
			return j;
		} else {
			System.out.print(matriz[i][j] + "  ");
			return metodoUno(i, j + 1, tam);
		}
	}


}
