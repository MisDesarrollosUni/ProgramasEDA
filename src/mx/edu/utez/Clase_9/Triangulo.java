package mx.edu.utez.Clase_9;

import java.util.Scanner;

public class Triangulo {

	private Scanner leer = new Scanner(System.in);
	private String[][] matriz;
	private int fila, columna;

	public void pedirNumeros() {
		System.out.print("   # de Filas (MÍN 3): ");
		fila = validarNumero();
		System.out.println();
		System.out.print("# de Columnas (MÍN 3): ");
		columna = validarNumeroIgual();
		matriz = new String[fila][columna];
		llenarEspacios();
		imprimirTriangulo();
	}

	public void llenarEspacios() {
		for (String[] matriz1 : matriz) {
			for (int j = 0; j < matriz1.length; j++) {
				matriz1[j] = "  ";
			}
		}
	}

	public int validarNumero() {
		int num = 0;
		boolean flag = false;
		do {
			try {
				num = leer.nextInt();
				if (num > 2) {
					flag = true;
				} else {
					System.out.print("   # (MÍN 3): ");
				}
			} catch (Exception e) {
				System.out.println("\tSOLO NÚMEROS");
				System.out.print("   # (MÍN 3): ");
				leer.next();
			}
		} while (!flag);
		return num;
	}

	public int validarNumeroIgual(){
		int num = 0;
		boolean flag = false;
		do {
			try {
				num = leer.nextInt();
				if (num == fila) {
					flag = true;
				} else {
					System.out.print("  # Deben ser Iguales: ");
				}
			} catch (Exception e) {
				System.out.println("\tSOLO NÚMEROS");
				System.out.print("  # Deben ser Iguales: ");
				leer.next();
			}
		} while (!flag);
		return num;
	}

	public void imprimirTriangulo(){
		int cont = 1;
		System.out.println("");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if(j<cont){
					System.out.print("*  ");
				}
			}
			cont++;
			System.out.println();
		}
	}
}
